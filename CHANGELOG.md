## [2.0.1](https://bitbucket.org/drewkates/sprawler/compare/v2.0.0...v2.0.1) (2019-04-27)

### Features

- **ChatLog:** Keep chat state in localstorage and allow keydown ([7c993f8](https://bitbucket.org/drewkates/sprawler/commits/7c993f8))

# [2.0.0](https://bitbucket.org/drewkates/sprawler/compare/v1.1.1...v2.0.0) (2019-04-27)

### Bug Fixes

- **styles:** minor updates to improve mobile experience ([25f5af2](https://bitbucket.org/drewkates/sprawler/commits/25f5af2))

### Features

- **App:** Adds Auth flow ([06b7f55](https://bitbucket.org/drewkates/sprawler/commits/06b7f55))
- **Auth:** Adds login flow ([3f86472](https://bitbucket.org/drewkates/sprawler/commits/3f86472))
- **character:** persist experience to database ([94c555f](https://bitbucket.org/drewkates/sprawler/commits/94c555f))
- **Character:** Add chat ([f736409](https://bitbucket.org/drewkates/sprawler/commits/f736409))
- **Character:** api for updating now works again ([47b6f3e](https://bitbucket.org/drewkates/sprawler/commits/47b6f3e))
- **cred:** Cred is persisted to db ([5eb1f4b](https://bitbucket.org/drewkates/sprawler/commits/5eb1f4b))
- **Harm:** Roll harm w/ modifier and log ([5c530ff](https://bitbucket.org/drewkates/sprawler/commits/5c530ff))
- **messageHandler:** Handle success by clearing textarea ([d516570](https://bitbucket.org/drewkates/sprawler/commits/d516570))

### BREAKING CHANGES

- **Auth:** Old structure no longer works. User can log in

## [1.1.1](https://bitbucket.org/drewkates/sprawler/compare/v1.1.0...v1.1.1) (2019-03-24)

# [1.1.0](https://bitbucket.org/drewkates/sprawler/compare/e6fa84f...v1.1.0) (2019-03-24)

### Bug Fixes

- **Abilities:** Fix dice rolling off-by-one error ([c55f75e](https://bitbucket.org/drewkates/sprawler/commits/c55f75e))
- **App:** Update hierarchy of navs to allow traversing back ([6c28682](https://bitbucket.org/drewkates/sprawler/commits/6c28682))
- **App.tsx:** Fix circular dependencies ([54e7120](https://bitbucket.org/drewkates/sprawler/commits/54e7120))

### Features

- **Abilities:** Adds ability to roll for specific move ([c82fc88](https://bitbucket.org/drewkates/sprawler/commits/c82fc88))
- **api:** Move api to separate directory ([3c74e72](https://bitbucket.org/drewkates/sprawler/commits/3c74e72))
- **Campaign:** Adds roll list and dynamic updates with socket ([f7c28c5](https://bitbucket.org/drewkates/sprawler/commits/f7c28c5))
- **roll:** adds roll logging to server ([e6fa84f](https://bitbucket.org/drewkates/sprawler/commits/e6fa84f))
- **RollBar:** Allow toggling of roll bar and loading more rolls ([7d4455c](https://bitbucket.org/drewkates/sprawler/commits/7d4455c))
