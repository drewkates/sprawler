import React, { Component, Fragment, MouseEvent } from 'react';
import { Auth } from '../../services/auth.service';
import { RouteComponentProps } from '@reach/router';
export class LandingPage extends Component<
  RouteComponentProps & { auth: Auth }
> {
  public render() {
    const { login, isAuthenticated, logout } = this.props.auth;
    return (
      <Fragment>
        <h1 style={{ margin: '10px auto', textAlign: 'center' }}>
          Welcome to Sprawler!
        </h1>
        {isAuthenticated() ? (
          <button onClick={logout}>Log Out</button>
        ) : (
          <button onClick={login}>Log In</button>
        )}

        <h2>Select an option:</h2>
        <ul>
          <li>Create a Character</li>
          <li>Access Existing Character</li>
        </ul>
      </Fragment>
    );
  }
}
