import React, { Component, Fragment } from 'react';
import { msgsSocket } from '../../utils/socket';
import api from '../../utils/api';
import styled from '@emotion/styled';
import { AddBtn } from '../StyledComponents';

export interface State {
  campaign_id: string;
  messages: any[];
  showMessages: boolean;
  currentMessage: string;
  dontScroll: boolean;
  chatScrollHeight: number;
  lastId?: string;
  lastTimestamp?: string;
  error?: boolean;
  rowNum: number;
}

interface Props {
  campaignId: string;
  handleMessageSend: (msg: string) => void;
}

const ChatTextInput = styled.textarea`
  border-bottom: 1px solid #fff;
  height: 50px;
  min-height: 50px;
  display: flex;
  flex: 3 0 auto;
  white-space: pre-line;
`;

const MsgDrafter = styled.div`
  bottom: 0;
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const SendMsgBtn = styled(AddBtn)`
  display: flex;
  flex: 1 0 auto;
  border-right: none;
  padding-right: 0;
  height: 50px;
  &:first-of-type {
    float: none;
  }
`;

const LoadMsgsBtn = styled(AddBtn)`
  display: block;
  margin: 0 auto;
  &:first-of-type {
    float: none;
  }
`;

export const SideBar = styled.div<{ open: boolean; side: string }>`
  overflow-y: scroll;
  overflow-x: hidden;
  transform: scaleX(${props => (props.open ? '1' : '0')});
  z-index: 5;
  background-color: #000;
  color: #fff;
  position: absolute;
  top: 0;
  ${props => props.side}: 0;
  display: block;
  height: auto;
  transition: transform 0.2s;
  min-width: 50%;
  max-width: 100%;
  min-height: 50%;
  max-height: calc(100% - 50px);
  border: 1px solid rgba(0, 255, 187, 0.5);
  box-shadow: -1px -2px 5px rgba(0, 255, 187, 0.4);
  border-right-width: 0;
`;

export const ChatButton = styled.button`
  position: fixed;
  bottom: 0;
  right: 0;
  border: 2px solid rgba(0, 255, 187,0.3);
  text-transform: uppercase;
  background: #000;
  color: #fff;
  box-shadow: 1px 1px 2px rgba(0,255,187,0.4), -1px -1px 2px rgba(0,255,187,0.4);
  height: 50px;
  width: 100px;
  transition: background-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
  cursor: pointer;
  font-size: 18px;
  &:hover {
    background-color: rgba(10,10,10,0.8);
    box-shadow: 2px 2px 10px rgba(0,255,187,0.4), -2px -2px 10px rgba(0,255,187,0.4);
  }
  z-index: 4;
}
`;

const ChatTable = styled.table`
  td {
    vertical-align: text-top;
  }
`;

const MessageEl = styled.tr<{ fresh: boolean }>`
  background-color: ${props => (props.fresh ? '#ddd' : '000')};
  transition: background-color 0.5s;
`;

const messageItem: (value: any) => JSX.Element = messageObj => {
  return (
    <MessageEl
      key={messageObj.id}
      fresh={messageObj.fresh}
      title={messageObj.created_at}
    >
      <td>
        <strong>{messageObj.playerName}</strong>
      </td>
      <td>{messageObj.text}</td>
    </MessageEl>
  );
};

export class ChatLogComponent extends Component<Props, State> {
  public state: State = {
    campaign_id: '',
    chatScrollHeight: 0,
    currentMessage: '',
    messages: [],
    dontScroll: false,
    showMessages: false,
    rowNum: 1
  };

  private api = api;
  private okToSet = true;
  private chatBoxRef: React.RefObject<HTMLDivElement>;
  constructor(props: Props) {
    super(props);
    this.chatBoxRef = React.createRef<HTMLDivElement>();
  }

  public getSnapshotBeforeUpdate(prevProps: Props, prevState: State) {
    if (this.okToSet) {
      if (this.chatBoxRef && this.chatBoxRef.current) {
        const { scrollHeight } = this.chatBoxRef.current;
        if (
          !this.state.chatScrollHeight ||
          (this.state.chatScrollHeight < scrollHeight && !this.state.dontScroll)
        ) {
          this.chatBoxRef.current.scrollTop = scrollHeight + 53;
          this.setState({
            chatScrollHeight: scrollHeight
          });
        }
      }
    }
    return null;
  }
  public componentDidUpdate(prev: any, prevS: any) {
    return null;
  }

  public handleMessageUpdate = (
    evt: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    this.setState({ currentMessage: evt.target.value });
  };

  private getDisplayMessages() {
    return (this.state.messages ? this.state.messages : []).map<any>(
      message => {
        const { fresh, id, created_at, player_name, message_text } = message;

        return {
          id,
          created_at: new Date(created_at).toLocaleString(),
          playerName: player_name,
          text: message_text,
          fresh
        };
      }
    );
  }

  public handleLoadMsgs = (ev: any) => {
    this.loadMoreMessages();
  };

  public handleKeyDown = (
    ev: React.KeyboardEvent<HTMLTextAreaElement>
  ): void => {
    if (ev.keyCode === 13 || ev.key === 'Enter') {
      if (ev.ctrlKey || ev.shiftKey) {
        this.setState({
          currentMessage: this.state.currentMessage + '\n'
        });
      } else {
        this.handleMessageSend(ev);
      }
    }
  };

  private clearUnread() {
    this.setState({
      messages: this.state.messages.map(m => {
        m.fresh = false;
        return m;
      })
    });
  }

  private addMessage(data: string): void {
    try {
      const newMessage = {
        ...JSON.parse(data),
        fresh: true
      };
      if (this.okToSet) {
        setTimeout(this.clearUnread.bind(this), 800);
        this.setState({
          messages: [...this.state.messages, newMessage]
        });
        if (!this.state.dontScroll) {
          this.updateChatScrollTop();
        }
      }
    } catch (e) {
      if (this.okToSet) {
        this.setState({ error: true });
      }
    }
  }

  public loadMoreMessages = async () => {
    const { lastTimestamp: prevTimestamp, lastId: prevId } = this.state;
    if (prevTimestamp && prevId && this.props.campaignId && this.okToSet) {
      const { messages, lastId, lastTimestamp } = await this.api.fetchMessages(
        this.props.campaignId,
        prevId,
        prevTimestamp,
        10
      );
      this.setState({
        messages: [...messages, ...this.state.messages],
        lastId,
        lastTimestamp
      });
      if (this.chatBoxRef && this.chatBoxRef.current) {
        this.chatBoxRef.current.scrollTop = 0;
      }
    }
  };
  public toggleShowMessages = (ev: any) => {
    if (this.okToSet) {
      const showMessages = !this.state.showMessages;
      localStorage.setItem('chatOpen', `${showMessages}`);
      this.setState({ showMessages });
    }
  };

  public async componentDidMount() {
    if (this.props.campaignId && this.okToSet) {
      const showMessages = localStorage.getItem('chatOpen') === 'true';
      const { messages, lastId, lastTimestamp } = await this.api.fetchMessages(
        this.props.campaignId,
        undefined,
        undefined,
        10
      );
      this.setState({
        showMessages,
        messages,
        lastId,
        lastTimestamp
      });

      msgsSocket.on('msg:added', this.addMessage.bind(this));
      msgsSocket.on('disconnect', () => {
        console.log('disconnected');
      });
    }
  }
  public componentWillUnmount() {
    this.okToSet = false;

    msgsSocket.off('msg:added');
    msgsSocket.off('disconnect');
  }
  public componentDidCatch() {
    console.error('error in chat log container');
    if (this.okToSet) {
      this.setState({
        error: true
      });
    }
  }

  public static getDerivedStateFromError(error: any) {
    // Update state so the next render will show the fallback UI.
    return { error: true };
  }

  public updateChatScrollTop() {
    if (this.chatBoxRef && this.chatBoxRef.current) {
      this.chatBoxRef.current.scrollTop += 53;
    }
  }

  public handleMessageSend = async (ev: any) => {
    this.props.handleMessageSend(this.state.currentMessage);
    this.setState({ currentMessage: '' });
    this.updateChatScrollTop();
  };

  public render() {
    const messages = this.getDisplayMessages();
    const { currentMessage, showMessages, rowNum } = this.state;

    return (
      <Fragment>
        <ChatButton onClick={this.toggleShowMessages}>Chat</ChatButton>
        <SideBar side="right" open={showMessages} ref={this.chatBoxRef}>
          <LoadMsgsBtn onClick={this.handleLoadMsgs}>
            Load More Messages
          </LoadMsgsBtn>
          <ChatTable>
            <tbody>{messages.map(messageItem)}</tbody>
          </ChatTable>
          <MsgDrafter className="new-msg-container">
            <ChatTextInput
              name="message-text"
              id="message-text"
              cols={30}
              rows={rowNum}
              value={currentMessage}
              onChange={this.handleMessageUpdate}
              onKeyDown={this.handleKeyDown}
              placeholder="Type message here..."
            />
            <SendMsgBtn onClick={this.handleMessageSend}>
              Send Message
            </SendMsgBtn>
          </MsgDrafter>
        </SideBar>
      </Fragment>
    );
  }
}
