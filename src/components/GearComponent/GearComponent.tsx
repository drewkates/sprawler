import React, { Component, FormEvent } from 'react';
import { Modal } from '../Modals';
import { AddModCtrl, InputFull, IconLabel } from '../StyledComponents';
import { evtNoop } from '../../utils/shared';
import { ModalWrapper } from '../Moves/MovesAndCyberware';
import { Gear } from '../../interfaces';

interface State {
  gear: Gear[];
  showModal: boolean;
  current_text: string;
}
interface Props {
  [key: string]: any;
}
export class GearComponent extends Component<Props, State> {
  public state: State = {
    gear: [],
    showModal: false,
    current_text: ''
  };

  constructor(props: Props) {
    super(props);
  }

  public fetchCharacters() {
    window.addEventListener('keydown', this.handleModalCancel.bind(this));
  }
  public componentWillUnmount() {
    window.removeEventListener('keydown', this.handleModalCancel.bind(this));
  }

  public makeGearList() {
    return this.state.gear.map((_: Gear, index: number) => {
      return (
        <fieldset key={index}>
          <div>
            <InputFull
              type="text"
              data-index={index}
              name={`attr_intelgear${index}`}
              value={_.details}
              readOnly={true}
            />
          </div>
        </fieldset>
      );
    });
  }

  public _toggleModal(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    this.setState({
      showModal: !this.state.showModal
    });
  }
  public toggleModal = this._toggleModal.bind(this);

  public _addGear(ev: FormEvent<HTMLButtonElement>): void {
    if (this.state.current_text.length > 0) {
      this.setState({
        gear: [...this.state.gear, { details: this.state.current_text }],
        showModal: false,
        current_text: ''
      });
    }
  }
  public addGear = this._addGear.bind(this);

  public _handleChange(ev: FormEvent<HTMLInputElement>) {
    this.setState({
      current_text: ev.currentTarget.value
    });
  }
  public handleChange = this._handleChange.bind(this);
  public handleModalCancel = (ev: KeyboardEvent | MouseEvent): void => {
    if (ev instanceof KeyboardEvent) {
      if (ev.keyCode === 27 && this.state.showModal) {
        this.setState({ showModal: false });
      }
    } else {
      this.setState({ showModal: false });
    }
  };

  public render() {
    const { showModal } = this.state;
    return (
      <div>
        {showModal ? (
          <Modal onCancel={this.handleModalCancel}>
            <ModalWrapper>
              <h1>Add gear:</h1>
              <InputFull type="text" onChange={this.handleChange} />
              <div className="buttons">
                {/*<AddBtn onClick={this.toggleModal}>No</AddBtn>*/}
                {/*<AddBtn onClick={this.addGear}>Yes</AddBtn>*/}
              </div>
            </ModalWrapper>
          </Modal>
        ) : null}
        <div>
          <IconLabel iconName="intel" htmlFor="attr_intelgear">
            [Intel] &amp; [Gear]
          </IconLabel>
        </div>

        {this.makeGearList()}
        <AddModCtrl>
          {/*<AddBtn onClick={this.toggleModal}>+Add</AddBtn>*/}
          {/*<AddBtn onClick={evtNoop}>Modify</AddBtn>*/}
        </AddModCtrl>
      </div>
    );
  }
}
