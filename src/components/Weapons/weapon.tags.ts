export type Tag =
  | 'Intimate'
  | 'Hand'
  | 'Close'
  | 'Near'
  | 'Far'
  | 'Ex'
  | 'AP'
  | 'area'
  | 'autofire'
  | 'breach'
  | 'clumsy'
  | 'dangerous'
  | 'discreet'
  | 'flechette'
  | 'linked'
  | 'loud'
  | 'messy'
  | 'numerous'
  | 'reload'
  | 'quick';

export const tags: Tag[] = [
  'Intimate',
  'Hand',
  'Close',
  'Near',
  'Far',
  'Ex',
  'AP',
  'area',
  'autofire',
  'breach',
  'clumsy',
  'dangerous',
  'discreet',
  'flechette',
  'linked',
  'loud',
  'messy',
  'numerous',
  'reload',
  'quick'
];
