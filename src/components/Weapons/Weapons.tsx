import React, { Component } from 'react';
import { Col6, IconLabel, EditableInput } from '../StyledComponents';
import { Weapon } from '../../interfaces';

interface WeaponsContainerProps {
  weapons: Weapon[];
}

export class Weapons extends Component<WeaponsContainerProps> {
  public render() {
    return (
      <div>
        <Col6>
          <IconLabel
            iconName="weapons"
            htmlFor="attr_equipment"
            style={{
              paddingLeft: '64px'
            }}
          >
            Weapons
          </IconLabel>
          {this.props.weapons.map(wep => {
            return (
              <fieldset key={wep.name}>
                <div>
                  <div>
                    <span>Name</span>
                  </div>
                  <div>
                    <EditableInput
                      value={wep.name}
                      readOnly={true}
                      canEdit={false}
                      type="text"
                    />
                  </div>
                </div>
                <div>
                  <div>
                    <span>Range</span>
                  </div>
                  <div>
                    <span>{wep.rangeTag}</span>
                  </div>
                </div>
                <div>
                  <div>
                    <span>Tags</span>
                  </div>
                  <div>
                    <ul>
                      {wep.tags.map(tag => {
                        return <li key={tag}>{tag}</li>;
                      })}
                    </ul>
                  </div>
                </div>
                <div>
                  <div>
                    <span>Harm</span>
                    <EditableInput
                      readOnly={true}
                      canEdit={false}
                      value={wep.harmRating}
                      style={{ marginLeft: '5px', width: '5em' }}
                    />
                  </div>
                  <div>
                    <span>Img Url</span>
                  </div>
                </div>
              </fieldset>
            );
          })}
          {/* <fieldset>
            <div>
              <div>
                <span>Name</span>
              </div>
              <div>
                <input type="text" />
              </div>
            </div>
            <div>
              <div>
                <span>Tags</span>
              </div>
              <div>
                <textarea name="attr_geartags" cols={25} rows={2} />
              </div>
            </div>
            <div>
              <div>
                <span>Harm</span>
              </div>
              <div>
                <input type="number" />
              </div>
              <div>
                <span>Img Url</span>
              </div>
              <div>
                <input type="text " name="" />
              </div>
            </div>
          </fieldset> */}
          {/*<AddBtn onClick={evtNoop}>+Add</AddBtn>*/}
          {/*<AddBtn onClick={evtNoop}>+Modify</AddBtn>*/}
        </Col6>
        <Col6>
          <IconLabel iconName="equipment" htmlFor="attr_equipment">
            Other Equipment
          </IconLabel>
          <fieldset>
            <div>
              <div>
                <input type="text" name="attr_equipname" />
              </div>
            </div>
          </fieldset>
        </Col6>
      </div>
    );
  }
}
