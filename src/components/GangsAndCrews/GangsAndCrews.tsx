import React, { Component, FormEvent } from 'react';
import { FlipSwitchComponent } from '../FlipSwitch';
import { Col6, BoldSpan } from '../StyledComponents';
import { Gang, Crew } from '../../interfaces';
import { ReactElementLike } from 'prop-types';
import { Collapse } from 'react-collapse';
interface State {
  showGangs: boolean;
  showCrews: boolean;
}
interface Props {
  [key: string]: any;
  gangs: Gang[];
  crews: Crew[];
}
export function GangComponent(gang: Gang): ReactElementLike {
  return (
    <div key={gang.id}>
      <div>
        <BoldSpan>Name: </BoldSpan>
        <span>{gang.gangName}</span>
      </div>
      <div>
        <BoldSpan>Type: </BoldSpan>
        <span>{gang.gangType}</span>
      </div>
      <div>
        <BoldSpan>Size: </BoldSpan>
        <span>{gang.gangSize}</span>
      </div>
      {/* <span>Territory: </span>
      <span>{}</span>
      <span>Leader: </span>
      <span>{}</span>
      <span>Main Gigs: </span>
      <span>{}</span>
      <span>Main Gigs: </span>
      <span>{}</span> */}
    </div>
  );
}

export function CrewComponent(crew: Crew): ReactElementLike {
  return (
    <div className="crew" key={crew.id}>
      <div>
        <BoldSpan>Name</BoldSpan>
        <span>{crew.crewName}</span>
      </div>
      <div>
        <BoldSpan>Type</BoldSpan>
        <span>{crew.crewType}</span>
      </div>
      <div>
        <BoldSpan>Profit</BoldSpan>
        <span>{crew.profit}</span>
      </div>
      <div>
        <BoldSpan>Disaster</BoldSpan>
        <span>{crew.disaster}</span>
      </div>
    </div>
  );
}

export class GangsAndCrews extends Component<Props, State> {
  public state: State = {
    showGangs: false,
    showCrews: false
  };
  private _handleGangsToggle(ev: FormEvent<HTMLInputElement>): void {
    this.setState({
      showGangs: !this.state.showGangs
    });
  }
  public handleGangsToggle = this._handleGangsToggle.bind(this);

  private _handleCrewsToggle(ev: FormEvent<HTMLInputElement>): void {
    this.setState({
      showCrews: !this.state.showCrews
    });
  }
  public handleCrewsToggle = this._handleCrewsToggle.bind(this);
  public render() {
    return (
      <div>
        <label htmlFor="attr_vehicles">Gangs and Crews</label>
        <div>
          <div>
            <FlipSwitchComponent
              checked={this.state.showGangs}
              handleChange={this.handleGangsToggle}
            />
            <div>
              <div>
                <Collapse isOpened={this.state.showGangs}>
                  <Col6>
                    <label htmlFor="attr_gangs">Gangs</label>
                    {this.props.gangs.map(GangComponent)}
                  </Col6>
                  <Col6>
                    <label htmlFor="attr_directives">Crews</label>
                    {/* {this.props.gangs} */}
                  </Col6>
                </Collapse>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
