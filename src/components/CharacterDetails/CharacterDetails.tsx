import React, { Component } from 'react';
import {
  FlexRow,
  InlineAttribute,
  EditableInput,
  ValueSpan,
  AttributeLabel
} from '../StyledComponents';
import { Characteristics, CharSheetState } from '../../interfaces';

type Props = Characteristics & { mode: CharSheetState };

export class CharacterDetails extends Component<Props> {
  public render() {
    const { eyes, face, body, wear, skin, mode } = this.props;
    const canEdit = mode === 'edit';
    return (
      <FlexRow>
        <InlineAttribute>
          <AttributeLabel htmlFor="attr_eyes">Eyes</AttributeLabel>
          {canEdit ? (
            <EditableInput
              canEdit={canEdit}
              type="text"
              name="attr_eyes"
              value={eyes}
              readOnly={true}
            />
          ) : (
            <ValueSpan>{eyes}</ValueSpan>
          )}
        </InlineAttribute>
        <InlineAttribute>
          <AttributeLabel htmlFor="attr_face">Face</AttributeLabel>
          {canEdit ? (
            <EditableInput
              canEdit={canEdit}
              type="text"
              name="attr_face"
              value={face}
              readOnly={true}
            />
          ) : (
            <ValueSpan>{face}</ValueSpan>
          )}
        </InlineAttribute>
        <InlineAttribute>
          <AttributeLabel htmlFor="attr_body">Body</AttributeLabel>
          {canEdit ? (
            <EditableInput
              canEdit={canEdit}
              type="text"
              name="attr_body"
              value={body}
              readOnly={true}
            />
          ) : (
            <ValueSpan>{body}</ValueSpan>
          )}
        </InlineAttribute>
        <InlineAttribute>
          <AttributeLabel htmlFor="attr_wear">Wear</AttributeLabel>
          {canEdit ? (
            <EditableInput
              canEdit={canEdit}
              type="text"
              name="attr_wear"
              value={wear}
              readOnly={true}
            />
          ) : (
            <ValueSpan>{wear}</ValueSpan>
          )}
        </InlineAttribute>
        <InlineAttribute>
          <AttributeLabel htmlFor="attr_skin">Skin</AttributeLabel>
          {canEdit ? (
            <EditableInput
              canEdit={canEdit}
              type="text"
              name="attr_skin"
              value={skin}
              readOnly={true}
            />
          ) : (
            <ValueSpan>{skin}</ValueSpan>
          )}
        </InlineAttribute>
      </FlexRow>
    );
  }
}
