import React, { Component, Fragment } from 'react';
import { Campaigns } from '../../containers/Campaigns';
import { LandingPage } from '../../components/LandingPage';
import { RouteComponentProps } from '@reach/router';
import { Auth } from '../../services/auth.service';

type Props = RouteComponentProps & { auth: Auth };
export class App extends Component<Props> {
  public login = () => {
    this.props.auth.login();
  };
  public logout = () => {
    this.props.auth.logout();
  };

  public componentDidMount() {
    const { renewSession, isAuthenticated } = this.props.auth;
    if (localStorage.getItem('isLoggedIn') === 'true' && !isAuthenticated()) {
      renewSession();
    }
  }

  public render() {
    const { isAuthenticated, profile } = this.props.auth;
    return isAuthenticated() ? (
      <Fragment>
        <h4>You are logged in!</h4>
        <pre>
          <code>{JSON.stringify(profile, null, 2)}</code>
        </pre>
        <button onClick={this.logout}>log out</button>
        <Campaigns />
      </Fragment>
    ) : (
      <LandingPage auth={this.props.auth} />
    );
  }
}
