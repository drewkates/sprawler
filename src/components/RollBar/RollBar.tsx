import React, { Component, Fragment } from 'react';
import { RouteComponentProps } from '@reach/router';
import { StyledLink, RollBar } from '../StyledComponents';
import { DBRollResult, DisplayRoll } from '../../interfaces';
import { rollsSocket } from '../../utils/socket';
import api from '../../utils/api';
import { ChatButton, SideBar } from '../MessageBar';

class ErrorBoundary extends Component<any, { hasError: boolean }> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false };
  }

  public static getDerivedStateFromError(error: any) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  public componentDidCatch(error: any, info: any) {
    // You can also log the error to an error reporting service
    console.error(error, info);
  }

  public render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

export interface State {
  campaign_id: string;
  rolls: DBRollResult[];
  msgs: any[];
  rollLimit: number;
  lastId?: string;
  lastTimestamp?: string;
  showRolls: boolean;
  error?: boolean;
}

const generateRollBar = (
  rollList: DisplayRoll[],
  cb: () => void,
  show: boolean
) => (
  <SideBar side="left" open={show}>
    <h4>Latest Rolls: </h4>
    <button onClick={cb}>Show more rolls!</button>
    <table>
      <thead>
        <tr>
          <th>Timestamp</th>
          <th>Player</th>
          <th>Ability</th>
          <th>Move</th>
          <th>Rolls</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {rollList.map(rollItem => {
          return (
            <tr key={rollItem.id}>
              <td>{rollItem.created_at.toLocaleString()}</td>
              <td>{rollItem.playerName}</td>
              <td>
                {rollItem.ability} ({rollItem.mod})
              </td>
              <td>{rollItem.moveName}</td>
              <td>{rollItem.rolls.join(',')}</td>
              <td>{rollItem.total}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </SideBar>
);
interface PlayerDeets {
  playerId: string;
  playerName: string;
}
interface Props {
  campaignId: string;
  players: PlayerDeets[];
}

export class RollBarComponent extends Component<Props, State> {
  public state: State = {
    campaign_id: '',
    rolls: [],
    msgs: [],
    rollLimit: 5,
    showRolls: false
  };

  private api = api;
  private okToSet = true;

  private getDisplayRolls() {
    return (this.state.rolls ? this.state.rolls : []).map<DisplayRoll>(roll => {
      const {
        id,
        roll_type: ability,
        values,
        roll_mod: mod,
        created_at,
        roll_move_name
      } = roll;

      return {
        id,
        ability,
        total: (values || []).reduce((a, b) => a + b, roll.roll_mod),
        rolls: values || [],
        mod,
        moveName: roll_move_name,
        created_at: new Date(created_at),
        playerName: ''
      };
    });
  }

  private addRoll(data: string): void {
    if (this.okToSet) {
      this.setState({
        rolls: [...this.state.rolls, JSON.parse(data)]
      });
    }
  }

  public loadMoreRolls = async () => {
    const { lastTimestamp: prevTimestamp, lastId: prevId } = this.state;
    if (prevTimestamp && prevId && this.props.campaignId && this.okToSet) {
      const { rolls, lastId, lastTimestamp } = await this.api.fetchRolls(
        this.props.campaignId,
        prevId,
        prevTimestamp,
        this.state.rollLimit
      );
      this.setState({
        rolls: [...this.state.rolls, ...rolls],
        lastId,
        lastTimestamp
      });
    }
  };
  public toggleShowRoll = (ev: any) => {
    if (this.okToSet) {
      this.setState({ showRolls: !this.state.showRolls });
    }
  };
  public async componentDidMount() {
    if (this.props.campaignId && this.okToSet) {
      const { rolls, lastId, lastTimestamp } = await this.api.fetchRolls(
        this.props.campaignId,
        undefined,
        undefined,
        this.state.rollLimit
      );
      this.setState({
        rolls,
        lastId,
        lastTimestamp
      });

      rollsSocket.on('roll:added', this.addRoll.bind(this));
      rollsSocket.on('disconnect', () => {
        console.log('disconnected');
      });
    }
  }
  public componentWillUnmount() {
    this.okToSet = false;

    rollsSocket.off('roll:added');
    rollsSocket.off('disconnect');
  }
  public componentDidCatch() {
    console.error('error in rollbar');
    if (this.okToSet) {
      this.setState({
        error: true
      });
    }
  }

  public static getDerivedStateFromError(error: any) {
    // Update state so the next render will show the fallback UI.
    return { error: true };
  }

  public render() {
    const rolls = this.getDisplayRolls();
    return (
      <ErrorBoundary>
        <Fragment>
          <ChatButton style={{ right: '110px' }} onClick={this.toggleShowRoll}>
            Rolls
          </ChatButton>
          {generateRollBar(rolls, this.loadMoreRolls, this.state.showRolls)}
        </Fragment>
      </ErrorBoundary>
    );
  }
}
