import React, { Component, FormEvent } from 'react';
import styled from '@emotion/styled';
import { ReactElementLike } from 'prop-types';
import { AddModCtrl, AddBtn, Col6, IconLabel } from '../StyledComponents';
import { Modal } from '../Modals';
import { Cyberware } from '../Cyberware';
import { CyberWareItem, ClientMove } from '../../interfaces';
import api from '../../utils/api';

interface State {
  playbook: string;
  showModal: boolean;
  showMoves: boolean;
  possibleMoves: ClientMove[];
}
interface Props {
  playbook: string;
  playerId: string;
  cyberware: CyberWareItem[];
  moves: ClientMove[];
}

const BoldSpan = styled.b`
  color: white;
`;

const MoveDiv = styled.div``;
export const ModalWrapper = styled.div`
  background: #0a2328;
  border: 1px #44e6cf solid;
  min-height: 200px;
  min-width: 300px;
  position: relative;
`;
interface ModalProps {
  cancel: (ev: FormEvent<HTMLButtonElement>) => void;
  add: (ev: FormEvent<HTMLButtonElement>) => void;
  onChange: (ev: FormEvent<HTMLSelectElement>) => void;
  playbook: string;
  possibleMoves: ClientMove[];
  currentMoves: ClientMove[];
}

export const MovesModalContent = (props: ModalProps) => {
  const moves = (props.possibleMoves || []).filter(move => {
    const currentHas = props.currentMoves.find(
      currMove => currMove.name === move.name
    );
    return !currentHas;
  });
  const selected = moves[0];
  // @ts-ignore
  return (
    <ModalWrapper>
      <h1>Add move:</h1>
      {selected ? (
        <div className="buttons">
          <select value={selected.name} onChange={props.onChange}>
            {moves.map((moveItem, ind) => (
              <option key={ind} value={moveItem.name}>
                {moveItem.name}
              </option>
            ))}
          </select>
          {selected.notes}
          {/*<AddBtn onClick={props.add}>Yes</AddBtn>*/}
          {/*<AddBtn onClick={props.cancel}>No</AddBtn>*/}
        </div>
      ) : null}
      {/*<AddBtn onClick={props.cancel}>Cancel</AddBtn>*/}
    </ModalWrapper>
  );
};

export class MovesAndCyberware extends Component<Props, State> {
  public api = api;
  public state: State = {
    showModal: false,
    playbook: '',
    showMoves: false,
    possibleMoves: []
  };
  constructor(props: Props) {
    super(props);
  }

  public canRender = true;

  public componentDidUpdate(prevProps: Props) {
    if (prevProps.playbook !== this.props.playbook) {
      this.tryGettingMovesByPlaybook(this.props.playbook);
    }
  }
  public async tryGettingMovesByPlaybook(playbook: string) {
    const possibleMoves = await this.api.fetchMovesByPlaybook(playbook);
    if (this.canRender) {
      this.setState({
        possibleMoves
      });
    }
  }
  public componentWillUnmount() {
    this.canRender = false;
  }

  public _toggleModal(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    this.setState({
      showModal: !this.state.showModal
    });
  }
  public _toggleMoves(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    this.setState({
      showMoves: !this.state.showMoves
    });
  }
  public toggleMoves = this._toggleMoves.bind(this);

  public _addGear(ev: FormEvent<HTMLButtonElement>): void {
    this.setState({
      showModal: false
    });
  }
  public addGear = this._addGear.bind(this);
  /*tslint:disable*/
  public _handleChangeMove(ev: FormEvent<HTMLSelectElement>): void {}
  /*tslint:enable*/
  public handleChangeMove = this._handleChangeMove.bind(this);
  public toggleModal = this._toggleModal.bind(this);

  public handleModalCancel = (ev: KeyboardEvent | MouseEvent): void => {
    if (
      ev instanceof KeyboardEvent &&
      ev.keyCode === 27 &&
      this.state.showModal
    ) {
      this.setState({ showModal: false });
    }
  };

  public getMoves(): ReactElementLike | ReactElementLike[] {
    const moves = this.props.moves || [];
    if (!moves || !moves.length) {
      return (
        <div>
          <BoldSpan>No Moves Found</BoldSpan>
        </div>
      );
    } else {
      return moves.map(move => (
        <MoveDiv key={move.name + 'move'}>
          <BoldSpan>{move.name}</BoldSpan>
          {move.notes}
        </MoveDiv>
      ));
    }
  }
  public getCyberwares() {
    return this.props.cyberware || [];
  }
  public render() {
    return (
      <div>
        {this.state.showModal ? (
          <Modal onCancel={this.handleModalCancel}>
            <MovesModalContent
              cancel={this.toggleModal}
              add={this.addGear}
              playbook={this.props.playbook.toLowerCase()}
              possibleMoves={this.state.possibleMoves}
              currentMoves={this.props.moves}
              onChange={this.handleChangeMove}
            />
          </Modal>
        ) : null}
        <Col6
          style={{
            paddingBottom: '20px'
          }}
        >
          <div>
            <IconLabel iconName="moves" htmlFor="attr_moves">
              Moves
            </IconLabel>
          </div>
          <AddBtn
            style={{ position: 'absolute', right: 0, top: 10 }}
            onClick={this.toggleMoves}
          >
            {this.state.showMoves ? 'Hide' : 'Show'} Moves
          </AddBtn>
          {this.state.showMoves ? this.getMoves() : null}
          <fieldset>
            <AddModCtrl>
              {/*<AddBtn onClick={this.toggleModal}>+Add</AddBtn>*/}
              {/*<AddBtn onClick={evtNoop}>Modify</AddBtn>*/}
            </AddModCtrl>
          </fieldset>
        </Col6>
        <Cyberware cyberware={this.getCyberwares()} />
      </div>
    );
  }
}
