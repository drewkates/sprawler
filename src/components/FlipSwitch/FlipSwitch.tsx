import React, { Component, FormEvent } from 'react';
import styled from '@emotion/styled';

const FlipSwitch = styled.input`
  opacity: 0;
  position: absolute;
  width: 68px;
  height: 27px;
  z-index: 10;
  :checked + .flipswitch-label .flipswitch-inner {
    margin-left: 0;
  }
  :checked + .flipswitch-label .flipswitch-dot {
    left: 50px;
  }
`;
const FlipSwitchLabel = styled.label`
  display: inline-block;
  overflow: hidden;
  cursor: pointer;
  border: 1px solid #4e948b;
  border-radius: 50px;
  padding: 0px;
  margin: 0px;
  box-shadow: 0px 0px 10px -3px #43e6cf;
  width: auto;
  max-width: 100%;
`;
const InnerFlipSwitch = styled.div`
  width: 200%;
  margin-left: -100%;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
  :before,
  :after {
    float: left;
    width: 50%;
    height: 24px;
    padding: 0;
    padding-right: 7px;
    padding-top: 1px;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    color: #97adaa;
    text-shadow: 0px 0px 14px rgb(0, 255, 187);
    text-transform: uppercase;
    letter-spacing: 2px;
    font-weight: 100;

    text-shadow: 0px 0px 14px rgb(0, 255, 187);
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
  }

  ::before {
    content: 'ON';
    padding-top: 4px;
    padding-left: 12px;
    background-color: #28c7c0;
    color: #0a2328;
  }

  ::after {
    content: 'OFF';
    padding-top: 4px;
    padding-right: 12px;
    background-color: #0a2328;
    color: #97adaa;
    text-align: right;
  }
`;
const InnerFlipSwitchDot = styled.div`
  width: 23px;
  height: 22px;
  margin: -0.5px;
  background: #ffffff;
  border: 2px solid #26928d;
  border-radius: 50px;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  box-shadow: 0px 0px 24px -3px #43e6cf;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  -ms-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
`;

interface Props {
  handleChange: (ev: FormEvent<HTMLInputElement>) => void;
  checked: boolean;
}
export class FlipSwitchComponent extends Component<Props> {
  public render() {
    return (
      <div style={{ position: 'relative' }}>
        <FlipSwitch
          className="flipswitch-input"
          name="attr_flipswitch"
          type="checkbox"
          checked={this.props.checked}
          onChange={this.props.handleChange}
        />
        <FlipSwitchLabel className="flipswitch-label">
          <InnerFlipSwitch className="flipswitch-inner" />
          <InnerFlipSwitchDot className="flipswitch-dot" />
        </FlipSwitchLabel>
      </div>
    );
  }
}
