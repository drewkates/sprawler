import React, { Component, FormEvent, Fragment } from 'react';
import { Col6, Row, IconLabel, BoldSpan } from '../StyledComponents';
import { FlipSwitchComponent } from '../FlipSwitch';
import { Vehicle, Profile } from './vehicle';
import { Drone } from '../../interfaces';
import { Collapse } from 'react-collapse';

interface State {
  showVehicles: boolean;
}

interface Props {
  vehicles: Vehicle[];
  drones: Drone[];
}

const Profile = (props: Profile) => (
  <Fragment>
    {Object.keys(props).map((key: keyof Profile, ind) => {
      return (
        <div key={ind}>
          {key}: {props[key]}
        </div>
      );
    })}
  </Fragment>
);

const VehicleComponent = (props: Vehicle) => (
  <div key={props.id}>
    <div>
      <div>
        <BoldSpan>Name: </BoldSpan>
        <span>{props.name}</span>
      </div>
    </div>
    <div>
      <div>
        <BoldSpan>Frame: </BoldSpan>
        <span>{props.frame}</span>
      </div>

      <div>
        <BoldSpan>Design: </BoldSpan>
        <span>{props.design}</span>
      </div>
    </div>
    <div>
      <div>
        <BoldSpan>Profile: </BoldSpan>
        <span>{props.profile}</span>
      </div>
    </div>
    <div>
      <div>
        <BoldSpan>Strengths: </BoldSpan>
        <span>{props.strengths.join(' | ')}</span>
      </div>
    </div>
    <div>
      <div>
        <BoldSpan>Looks: </BoldSpan>
        <span>{props.looks.join(' | ')}</span>
      </div>
    </div>
    <div>
      <div>
        <BoldSpan>Weaknesses: </BoldSpan>
        <span>{props.weaknesses.join(' | ')}</span>
      </div>
    </div>
    <div>
      <div>
        <BoldSpan>Weapons: </BoldSpan>
        <span>{props.weapons.join(' | ')}</span>
      </div>
    </div>
  </div>
);

// const Drone = (props: Drone) => (
//   <div>
//     <div>
//       <div>
//         <span>Name</span>
//       </div>
//       <div>
//         <span>{props.name}</span>
//       </div>
//     </div>
//     <div>
//       <div>
//         <span>Frame</span>
//       </div>
//       <div>
//         <span>{props.frame}</span>
//       </div>
//       <div>
//         <span>Design</span>
//       </div>
//       <div>
//         <span>{props.design}</span>
//       </div>
//     </div>
//     <div>
//       <div>
//         <span>Profile</span>
//       </div>
//       <div>{props.profile ? Profile(props.profile) : 'No Profile'}</div>
//     </div>
//     <div>
//       <div>
//         <span>Strengths</span>
//       </div>
//       <div>
//         <span>{props.strengths.join('|')}</span>
//       </div>
//     </div>
//     <div>
//       <div>
//         <span>Looks</span>
//       </div>
//       <div>
//         <span>{props.looks.join('|')}</span>
//       </div>
//     </div>
//     <div>
//       <div>
//         <span>Weaknesses</span>
//       </div>
//       <div>
//         <span>{props.weaknesses.join('|')}</span>
//       </div>
//     </div>
//     <div>
//       <div>
//         <span>Weapons</span>
//       </div>
//       <div>
//         <span>{props.weapons.join('|')}</span>
//       </div>
//     </div>
//   </div>
// );
export class Vehicles extends Component<Props, State> {
  public state: State = {
    showVehicles: false
  };
  private _handleVehicleToggle(ev: FormEvent<HTMLInputElement>) {
    this.setState({
      showVehicles: !this.state.showVehicles
    });
  }
  public handleVehicleToggle = this._handleVehicleToggle.bind(this);
  public render() {
    return (
      <div>
        <label htmlFor="attr_vehicles">Vehicles and Drones</label>
        <div>
          <div>
            <FlipSwitchComponent
              handleChange={this.handleVehicleToggle}
              checked={this.state.showVehicles}
            />
            <Collapse isOpened={this.state.showVehicles}>
              <Row>
                <Col6>
                  <div>
                    <IconLabel
                      iconName="vehicles"
                      htmlFor="attr_equipment"
                      style={{
                        paddingLeft: '42px',
                        color: '#f3a332',
                        textShadow: '0 0 15px #ff935f'
                      }}
                    >
                      Vehicles
                    </IconLabel>
                  </div>
                  {this.props.vehicles.map(VehicleComponent)}
                </Col6>
                <Col6>
                  <IconLabel
                    iconName="drones"
                    htmlFor="attr_directives"
                    style={{
                      paddingLeft: '42px',
                      color: '#f3a332',
                      textShadow: '0 0 15px #ff935f'
                    }}
                  >
                    Drones
                  </IconLabel>
                </Col6>
              </Row>
            </Collapse>
          </div>
        </div>
      </div>
    );
  }
}
