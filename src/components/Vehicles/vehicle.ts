export interface Vehicle {
  id: string;
  tags: string[];
  name: string;
  frame: string;
  design: string;
  looks: string[];
  strengths: string[];
  weaknesses: string[];
  profile?: string;
  weapons: string[];
}

export interface Profile {
  power: number;
  looks: number;
  weakness: number;
  armor: number;
  [key: string]: number;
}
export const profiles: Profile[] = [
  { power: 2, looks: 1, weakness: 1, armor: 1 },
  { power: 2, looks: 2, weakness: 1, armor: 0 },
  { power: 1, looks: 2, weakness: 1, armor: 1 },
  { power: 2, looks: 1, weakness: 2, armor: 2 }
];

export const frames = [
  'motorcycle',
  'car',
  'hovercraft',
  'boat',
  'vectored thrust panzer',
  'fixed-wing aircraft',
  'helicopter'
];

export const designs = [
  'racing',
  'recreational',
  'transportation',
  'cargo',
  'military',
  'luxury',
  'civilian',
  'commercial',
  'courier'
];

export const looks = [
  'sleek',
  'vintage',
  'pristine',
  'powerful',
  'luxurious',
  'flashy',
  'muscular',
  'quirky',
  'pretty',
  'garish',
  'armoured',
  'armed',
  'nondescript'
];

export const strengths = [
  'fast',
  'quiet',
  'rugged',
  'aggressive',
  'huge',
  'off-road',
  'responsive',
  'uncomplaining',
  'capacious',
  'workhorse',
  'easily repaired'
];

export const weaknesses = [
  'slow',
  'fragile',
  'unresponsive',
  'lazy',
  'cramped',
  'picky',
  'guzzler',
  'unreliable',
  'loud'
];
