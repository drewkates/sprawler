import React, { Component, FormEvent } from 'react';
import { FlipSwitchComponent } from '../FlipSwitch';
import { Collapse } from 'react-collapse';
interface State {
  showCyberdeck: boolean;
}
export class CyberdeckComponent extends Component<
  { [key: string]: any },
  State
> {
  public state: State = {
    showCyberdeck: false
  };
  private _handleFlipswitchChange(ev: FormEvent<HTMLInputElement>) {
    this.setState({
      showCyberdeck: !this.state.showCyberdeck
    });
  }
  public handleFlipswitchChange = this._handleFlipswitchChange.bind(this);
  public render() {
    return (
      <div>
        <label htmlFor="attr_vehicles">Cyberdeck</label>
        <div>
          <div>
            <FlipSwitchComponent
              handleChange={this.handleFlipswitchChange}
              checked={this.state.showCyberdeck}
            />
            <Collapse isOpened={this.state.showCyberdeck}>
              <div>
                <div>
                  <div>
                    <label htmlFor="attr_cyberdeck">Cyberdeck</label>
                  </div>
                  <div>
                    <div>
                      <span>Name</span>
                    </div>
                    <div>
                      <input type="text" name="attr_cyberdeck-name" />
                    </div>
                  </div>
                  <div>
                    <div>
                      <span>Hardening</span>
                    </div>
                    <div>
                      <input type="number" name="attr_cyberdeck-hard" />
                    </div>
                    <div>/</div>
                    <div>
                      <input type="number" name="attr_cyberdeck-hardmax" />
                    </div>
                    <div>
                      <span>Firewall</span>
                    </div>
                    <div>
                      <input type="number" name="attr_cyberdeck-fire" />
                    </div>
                    <div>/</div>
                    <div>
                      <input type="number" name="attr_cyberdeck-firemax" />
                    </div>
                  </div>
                  <div>
                    <div>
                      <span>Processor</span>
                    </div>
                    <div>
                      <input type="number" name="attr_cyberdeck-pro" />
                    </div>
                    <div>/</div>
                    <div>
                      <input type="number" name="attr_cyberdeck-promax" />
                    </div>
                    <div>
                      <span>Stealth</span>
                    </div>
                    <div>
                      <input type="number" name="attr_cyberdeck-stealth" />
                    </div>
                    <div>/</div>
                    <div>
                      <input type="number" name="attr_cyberdeck-stealthmax" />
                    </div>
                  </div>
                  <div>
                    <div>
                      <span>Programs Loaded</span>
                    </div>
                    <div>
                      <input type="text" name="attr_cyberdeck-loaded" />
                    </div>
                  </div>
                  <div>
                    <div>
                      <span>Avatar Img</span>
                    </div>
                    <div>
                      <input type="text" name="attr_cyberdeck-imgurl" />
                    </div>
                  </div>
                </div>
                <div>
                  <div>
                    <label htmlFor="attr_programs">Programs Owned</label>
                  </div>
                  <fieldset>
                    <div>
                      <div>
                        <input type="text" name="attr_programs" />
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>
            </Collapse>
          </div>
        </div>
      </div>
    );
  }
}
