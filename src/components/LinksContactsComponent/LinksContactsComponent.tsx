import React, { Component, FormEvent } from 'react';
import {
  Col6,
  AddBtn,
  IconLabel,
  Row,
  EditableInput,
  NumInput,
  BoldSpan,
  DescP
} from '../StyledComponents';
import { LinksDB, ContactDetails } from '../../interfaces';
import { ReactElementLike } from 'prop-types';
export interface Link {
  characterName: string;
  linksValue: number;
}
interface State {
  links: Link[];
  currentLinksName: string;
  currentLinksValue: number | undefined;
  editMode: boolean;
}
interface Props {
  [key: string]: any;
  links: LinksDB[];
  contacts: ContactDetails[];
}

export function Contact(props: ContactDetails): ReactElementLike {
  return (
    <div key={props.name} style={{ marginLeft: '10px' }}>
      <BoldSpan>{props.name}</BoldSpan>
      <DescP>
        <strong>Description: </strong>
        {props.description}
      </DescP>
      <DescP>
        <strong>Background: </strong>
        {props.background}
      </DescP>
    </div>
  );
}

export class LinksContactsComponent extends Component<Props, State> {
  public state: State = {
    links: [],
    currentLinksName: '',
    currentLinksValue: 0,
    editMode: false
  };

  private _handleLinkNameChange(ev: FormEvent<HTMLInputElement>): void {
    ev.preventDefault();
    this.setState({
      currentLinksName: ev.currentTarget.value
    });
  }
  public handleLinkNameChange = this._handleLinkNameChange.bind(this);

  private _handleLinksValueChange(ev: FormEvent<HTMLInputElement>): void {
    ev.preventDefault();

    const newLinksValue = ev.currentTarget.value;
    if (newLinksValue === '') {
      this.setState({
        currentLinksValue: undefined
      });
    }
    this.setState({
      currentLinksValue: parseInt(newLinksValue, 10)
    });
  }
  private handleLinksValueChange = this._handleLinksValueChange.bind(this);

  public _handleAddLink(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    const newLinks = this.state.links;
    const newLinkValue: number | undefined =
      this.state.currentLinksValue === undefined
        ? 0
        : this.state.currentLinksValue;
    if (this.state.currentLinksName) {
      newLinks.push({
        characterName: this.state.currentLinksName,
        linksValue: newLinkValue
      });
    }

    this.setState({
      links: newLinks,
      currentLinksName: '',
      currentLinksValue: 0
    });
  }
  public handleAddLink = this._handleAddLink.bind(this);

  private _toggleEditMode(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    this.setState({
      editMode: !this.state.editMode
    });
  }
  public toggleEditMode = this._toggleEditMode.bind(this);

  public render() {
    return (
      <Col6>
        <Row>
          <div>
            <IconLabel iconName="links" htmlFor="attr_links">
              Links
            </IconLabel>
            {this.props.links.map((link, linkIndex) => {
              return (
                <div key={linkIndex}>
                  <fieldset>
                    <EditableInput
                      canEdit={false}
                      value={link.playerName}
                      readOnly={true}
                    />
                    <NumInput
                      canEdit={false}
                      value={link.quant}
                      readOnly={true}
                      type="number"
                      name="attr_links"
                      style={{
                        marginLeft: '20px'
                      }}
                    />
                  </fieldset>
                </div>
              );
            })}
            {/* <fieldset>
              <input
                type="text"
                name="attr_linkname"
                value={this.state.currentLinksName}
                onChange={this.handleLinkNameChange}
                placeholder="Enter name here"
              />
              <input
                type="number"
                name="attr_links"
                value={this.state.currentLinksValue}
                onChange={this.handleLinksValueChange}
                style={{
                  marginLeft: '20px'
                }}
              />
            </fieldset> */}
          </div>
        </Row>
        {/*<AddBtn onClick={this.handleAddLink}>+Add</AddBtn>*/}
        {/*<AddBtn onClick={this.toggleEditMode}>+Modify</AddBtn>*/}
        <Row style={{ borderTop: 'none', boxShadow: 'none' }}>
          <IconLabel iconName="contacts" htmlFor="attr_contacts">
            Contacts
          </IconLabel>
          {this.props.contacts.map(c => Contact(c))}
        </Row>
      </Col6>
    );
  }
}
