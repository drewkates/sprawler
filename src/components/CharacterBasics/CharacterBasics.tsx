import React, { Component } from 'react';
import {
  FlexRow,
  Col4,
  CustomSelect,
  NumInput,
  EditableInput,
  ValueSpan,
  AttributeLabel
} from '../StyledComponents';
import { evtNoop } from '../../utils/shared';
import { ReactElementLike } from 'prop-types';
import { CharSheetState } from '../../interfaces';

const playbooks = [
  'driver',
  'fixer',
  'hacker',
  'hunter',
  'infiltrator',
  'killer',
  'pusher',
  'reporter',
  'soldier',
  'tech'
].map(txt => txt.slice(0, 1).toUpperCase() + txt.slice(1));
function getPlaybooks(currentPlaybook: string): ReactElementLike[] {
  return playbooks.map((pb: string) => (
    <option key={pb} value={pb.toUpperCase()}>
      {pb}
    </option>
  ));
}

interface Props {
  player_name: string;
  playbook: string;
  experience: number;
  imgURL: string;
  mode: CharSheetState;
  onXPChange: (xp: number) => void;
  [key: string]: any;
}

export class CharacterBasics extends Component<Props> {
  public getSelectOrNone() {
    if (this.props.mode === 'edit') {
      return (
        <CustomSelect
          name="playbook"
          onChange={evtNoop}
          value={this.props.playbook.toUpperCase()}
        >
          {getPlaybooks(this.props.playbook)}
        </CustomSelect>
      );
    } else {
      return <ValueSpan>{this.props.playbook}</ValueSpan>;
    }
  }

  public onXPUpdate = (ev: React.ChangeEvent<HTMLInputElement>) => {
    const xp = parseInt(ev.currentTarget.value, 10);
    this.props.onXPChange(xp);
  };

  public render() {
    const canEdit = this.props.mode === 'edit';
    return (
      <FlexRow>
        <Col4>
          <AttributeLabel htmlFor="attr_basics">Name</AttributeLabel>
          {canEdit ? (
            <EditableInput
              canEdit={canEdit}
              type="text"
              name="player_name"
              value={this.props.player_name}
              onChange={evtNoop}
              readOnly={this.props.mode !== 'edit'}
            />
          ) : (
            <ValueSpan>{this.props.player_name}</ValueSpan>
          )}
        </Col4>
        <Col4>
          <AttributeLabel htmlFor="attr_basics">Playbook</AttributeLabel>
          {this.getSelectOrNone()}
        </Col4>
        <Col4>
          <AttributeLabel htmlFor="attr_basics">Experience</AttributeLabel>
          <NumInput
            canEdit={true}
            type="number"
            name="attr_xp"
            value={this.props.experience}
            onChange={this.onXPUpdate}
            style={{
              width: '3.5em'
            }}
          />
        </Col4>
        <Col4>
          <AttributeLabel htmlFor="attr_fileimg">IMG URL</AttributeLabel>
          {canEdit ? (
            <EditableInput
              canEdit={canEdit}
              type="text"
              name="attr_fileimg"
              value={this.props.imgURL}
              onChange={evtNoop}
              readOnly={this.props.mode !== 'edit'}
              style={{ width: '67%' }}
            />
          ) : (
            <ValueSpan>{this.props.imgURL}</ValueSpan>
          )}
        </Col4>
      </FlexRow>
    );
  }
}
