import React, { Component } from 'react';
import { Auth } from '../../services/auth.service';
import { RouteComponentProps } from '@reach/router';

export class Callback extends Component<RouteComponentProps & { auth: Auth }> {
  private _authorizeIfAuthenticated(hash: string): void {
    const { handleAuthentication } = this.props.auth;
    if (/id_token|acces_token/.test(hash)) {
      handleAuthentication();
    }
  }
  public authorizeIfAuthenticated = this._authorizeIfAuthenticated.bind(this);

  public render() {
    if (this.props.location) {
      this.authorizeIfAuthenticated(this.props.location.href);
    }
    return <div>loading...</div>;
  }
}
