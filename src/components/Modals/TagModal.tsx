import React, { Component } from 'react';
import { createPortal } from 'react-dom';

interface Props {
  onCancel?: (ev: KeyboardEvent) => void;
}
export class TagModal extends Component<Props> {
  private el = document.createElement('div');
  private modalRoot: HTMLElement | null;
  constructor(props: any) {
    super(props);
    this.modalRoot = document.getElementById('tag-modal');
    const noop = (ev: KeyboardEvent) => void 0;
  }

  public componentDidMount() {
    if (this.modalRoot) {
      this.modalRoot.appendChild(this.el);
    }
  }
  public componentWillUnmount() {
    if (this.modalRoot) {
      this.modalRoot.removeChild(this.el);
    }
  }
  public render() {
    return createPortal(this.props.children, this.el);
  }
}

export default TagModal;
