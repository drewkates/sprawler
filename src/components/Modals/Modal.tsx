import React, { Component } from 'react';
import { createPortal } from 'react-dom';

interface Props {
  onCancel?: (ev: MouseEvent | KeyboardEvent) => void;
}
export class Modal extends Component<Props> {
  private el = document.createElement('div');
  private modalRoot: HTMLElement | null;
  private handleCancel: (ev: KeyboardEvent | MouseEvent) => void;

  public handleClick = (ev: MouseEvent) => {
    if (this.handleCancel && ev.target === this.modalRoot) {
      this.handleCancel(ev);
    }
  };

  public handleKey = (ev: KeyboardEvent) => {
    if (this.handleCancel) {
      this.handleCancel(ev);
    }
  };

  constructor(props: any) {
    super(props);
    this.modalRoot = document.getElementById('modal');
    const noop = () => void 0;
    this.handleCancel = this.props.onCancel || noop;
  }

  public componentDidMount() {
    if (this.modalRoot) {
      this.modalRoot.appendChild(this.el);
      window.addEventListener('keydown', this.handleKey);
      this.modalRoot.addEventListener('click', this.handleClick);
    }
  }
  public componentWillUnmount() {
    if (this.modalRoot) {
      this.modalRoot.removeChild(this.el);
      window.removeEventListener('keydown', this.handleKey);
      this.modalRoot.removeEventListener('click', this.handleClick);
    }
  }
  public render() {
    return createPortal(this.props.children, this.el);
  }
}

export default Modal;
