import React, { Component } from 'react';
import { Row } from '../StyledComponents';

export class Notes extends Component {
  public render() {
    return (
      <Row>
        <label htmlFor="attr_notes">Notes</label>
        <textarea name="attr_notes" cols={50} rows={10} />
      </Row>
    );
  }
}
