import React, { Component, FormEvent, Fragment } from 'react';
import { Col6, AddBtn, IconLabel, HarmRadioLabel } from '../StyledComponents';
import styled from '@emotion/styled';
import harm_1200 from '../../media/img/harm_1200.jpg';
import harm_1500 from '../../media/img/harm_1500.jpg';
import harm_1800 from '../../media/img/harm_1800.jpg';
import harm_2100 from '../../media/img/harm_2100.jpg';
import harm_2200 from '../../media/img/harm_2200.jpg';
import harm_2300 from '../../media/img/harm_2300.jpg';
import harm_0000 from '../../media/img/harm_0000.jpg';
const harmImages: {
  [key: string]: any;
} = {
  '1200': harm_1200,
  '1500': harm_1500,
  '1800': harm_1800,
  '2100': harm_2100,
  '2200': harm_2200,
  '2300': harm_2300,
  '0000': harm_0000
};
const HarmClock = styled.div<{
  level: string;
}>`
  width: 100%;
  transition-property: background-image;
  transition: background-image 1s ease-in-out;
  height: 68px;
  background-image: url(${props => harmImages[props.level]});
  background-size: contain;
  background-repeat: no-repeat;

  @media screen and (max-width: 400px) {
    width: 350px;
    background-position-x: -4px;
  }
`;
const Harm = styled.div`
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  box-sizing: border-box;
  color: #44e6cf;
  display: flex;
  flex-flow: wrap;
  font-weight: 100;
  height: 87.2656px;
  justify-content: space-between;
  letter-spacing: 1px;
  line-height: 12.0714px;
  text-shadow: 0 0 15px rgb(0, 255, 187);
  text-size-adjust: 100%;
  text-transform: uppercase;
  user-select: none;
  white-space: nowrap;
  width: 100%;
`;
const ClockBox = styled.div`
  font-size: 10px;
  align-self: center;
  display: flex;
`;
const HarmInput = styled.input`
  -webkit-appearance: radio;
  -webkit-rtl-ordering: logical;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  background-color: rgb(18, 49, 56);
  border-width: 1px 0 0 0;
  border-color: rgb(240, 255, 252);
  color: rgb(240, 255, 252);
  cursor: pointer;
  display: inline-block;
  font-size: 6.76px;
  font-weight: 100;
  height: 13px;
  width: 13px;
  left: 0px;
  letter-spacing: 2px;
  margin-bottom: 0px;
  margin-left: 0px;
  margin-right: 0px;
  margin-top: 0px;
  opacity: 0;
  padding: 0;
  position: relative;
  text-transform: uppercase;
  top: -6px;
  user-select: none;
  vertical-align: middle;
  white-space: nowrap;
  word-spacing: 0px;
  z-index: 10;
  :checked + label.radio-label::before {
    opacity: 1;
  }
`;

const HarmGroupContainer = styled.div`
  display: flex;
`;

interface HarmProps {
  onRadio: (ev: React.ChangeEvent<HTMLInputElement>) => any;
  level: string;
  index: number;
  value: string;
}

const HarmGroup = (props: HarmProps) => (
  <HarmGroupContainer>
    <HarmInput
      type="radio"
      id={`harm${props.index}`}
      onChange={props.onRadio}
      checked={props.level === props.value}
      value={props.value}
    />
    <HarmRadioLabel htmlFor={`harm${props.index}`} className="radio-label" />{' '}
    <ClockBox>
      {props.value
        .split('')
        .slice(0, 2)
        .join('')}
      :
      {props.value
        .split('')
        .slice(2)
        .join('')}
    </ClockBox>{' '}
  </HarmGroupContainer>
);

export interface State {
  radioSelection: string;
}

interface Props {
  [key: string]: any;
  level: string;
  handleChange: (newHarm: string) => void;
  handleRoll: (props: {
    rollMod: string;
    rollType: string;
    values: number[];
    moveName: any;
  }) => void;
}

export class HarmComponent extends Component<Props, State> {
  public state: State = {
    radioSelection: '1200'
  };
  constructor(props: Props) {
    super(props);
  }
  private _onRadio(ev: FormEvent<HTMLInputElement>) {
    this.setState({
      radioSelection: ev.currentTarget.value
    });
    if (this.props.handleChange) {
      this.props.handleChange(ev.currentTarget.value);
    }
  }
  public roll = this._roll.bind(this);
  public _roll() {
    const rolls = Array(2)
      .fill(1)
      .map(() => Math.ceil(Math.random() * 6));
    const currentHarm =
      Object.keys(harmImages).indexOf(this.state.radioSelection) + 1;
    const res = rolls.reduce((a, b) => a + b, currentHarm);
    this.props.handleRoll({
      moveName: '',
      rollMod: `${currentHarm}`,
      rollType: 'Harm',
      values: rolls
    });
  }
  public onRadio = this._onRadio.bind(this);
  public render() {
    const level = this.props.level.replace(':', '');
    return (
      <Col6>
        <IconLabel iconName="harm" customLeftPad="26px">
          Harm
        </IconLabel>
        <Harm>
          <HarmGroup
            value={'1200'}
            index={0}
            level={level}
            onRadio={this.onRadio}
          />
          <HarmGroup
            value={'1500'}
            index={1}
            level={level}
            onRadio={this.onRadio}
          />
          <HarmGroup
            value={'1800'}
            index={2}
            level={level}
            onRadio={this.onRadio}
          />
          <HarmGroup
            value={'2100'}
            index={3}
            level={level}
            onRadio={this.onRadio}
          />
          <HarmGroup
            value={'2200'}
            index={4}
            level={level}
            onRadio={this.onRadio}
          />
          <HarmGroup
            value={'2300'}
            index={5}
            level={level}
            onRadio={this.onRadio}
          />
          <HarmGroup
            value={'0000'}
            index={6}
            level={level}
            onRadio={this.onRadio}
          />
          <HarmClock level={level} />
        </Harm>
        <AddBtn
          onClick={this.roll}
          style={{
            padding: '10px'
          }}
        >
          Roll Harm
        </AddBtn>
      </Col6>
    );
  }
}
