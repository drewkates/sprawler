import React, { Component, MouseEvent } from 'react';
import { CharacterBasics } from '../CharacterBasics';
import { CharacterDetails } from '../CharacterDetails';
import { Abilities } from '../Abilities';
import { CredAndHarm } from '../CredAndHarm';
import { MovesAndCyberware } from '../Moves/MovesAndCyberware';
import { LinksContactsComponent } from '../LinksContactsComponent';
import { Weapons } from '../Weapons';
import { Vehicles, Vehicle } from '../Vehicles';
import { CyberdeckComponent } from '../Cyberdeck';
import { GangsAndCrews } from '../GangsAndCrews';
import { Notes } from '../Notes';
import { Row, AddBtn, StyledLink } from '../StyledComponents';
import { DirectivesComponent } from '../DirectivesComponent';
import Api, { parseDBMoves, RollLog } from '../../utils/api';
import { cyberwareOptions } from '../../cached_data/cyberware_options';
import { RouteComponentProps } from '@reach/router';
import {
  ClientMove,
  CyberWareItem,
  CharacterState,
  CharSheetState,
  ContactDB,
  ContactDetails,
  Drone
} from '../../interfaces';

type State = CharacterState & { mode: CharSheetState; messageText: string };

interface CharacterProps
  extends RouteComponentProps<{ characterId: string; campaignId: string }> {}

function formatContact(dbContact: ContactDB): ContactDetails {
  return {
    background: dbContact.contact_background,
    description: dbContact.contact_description,
    name: dbContact.contact_name
  };
}

export class Character extends Component<CharacterProps, State> {
  public state: State = {
    body: '',
    cool: 0,
    cred: 0,
    crews: [],
    contacts: [],
    cyberware: [],
    directives: [],
    drones: [],
    edge: 0,
    experience: 0,
    eyes: '',
    face: '',
    gangs: [],
    gear: [],
    harm: '0000',
    id: '',
    links: [],
    meat: 0,
    mind: 0,
    mode: 'read',
    moves: [],
    playbook: '',
    player_name: '',
    skin: '',
    style: 0,
    synth: 0,
    vehicles: [],
    weapons: [],
    wear: '',
    messageText: ''
  };
  public destroyedBeforeFetch: boolean = false;
  private api = Api;

  constructor(props: CharacterProps) {
    super(props);
  }

  public async fetchCharacter() {
    const characterId = this.props.characterId || '';

    const char = await this.api.fetchCharacter(characterId);
    if (!char || !this.canSetState) {
      return;
    }
    if (this.canSetState) {
      this.setState({
        playbook: char.playbook,
        player_name: char.player_name,
        experience: char.experience,
        eyes: char.eyes || '',
        face: char.face || '',
        body: char.body || '',
        wear: char.wear || '',
        skin: char.skin || '',
        cool: char.cool,
        edge: char.edge,
        meat: char.meat,
        mind: char.mind,
        style: char.style,
        synth: char.synth,
        cred: char.cred,
        harm: char.harm
      });
      const cyberwareRes = await this.api.fetchPlayerCyberware(characterId);
      const cyberware = cyberwareRes.map(ware => {
        const formattedWare: CyberWareItem = {
          name: ware.name,
          details: cyberwareOptions[ware.name].details,
          tags: ware.tags
        };
        return formattedWare;
      });
      const links = await this.api.fetchPlayerLinks(characterId);
      const weapons = await this.api.fetchPlayerWeapons(characterId);
      const directives = await this.api.fetchPlayerDirectives(characterId);
      const contacts = await this.api.fetchPlayerContacts(characterId);
      const moves = (await this.api.fetchPlayerMoves(characterId)).map<
        ClientMove
      >(parseDBMoves);
      const gangs = await this.api.fetchPlayerGangs(characterId);
      const crews = await this.api.fetchPlayerCrews(characterId);
      const vehicles = await this.api.fetchDetail<Vehicle[]>(
        characterId,
        'vehicle'
      );
      const drones = await this.api.fetchDetail<Drone[]>(characterId, 'drone');

      if (this.canSetState) {
        this.setState({
          contacts,
          crews,
          cyberware,
          drones,
          gangs,
          moves,
          links,
          weapons,
          directives,
          vehicles
        });
      }
    }
  }

  public componentDidUpdate(prevProps: CharacterProps) {
    if (
      this.props.characterId &&
      prevProps.characterId !== this.props.characterId
    ) {
      this.fetchCharacter();
    }
  }

  public componentDidMount() {
    if (this.props.characterId) {
      this.fetchCharacter();
    }
  }

  public canSetState = true;
  public componentWillUnmount() {
    this.canSetState = false;
  }
  public toggleMode = (ev: MouseEvent) => {
    if (this.canSetState) {
      this.setState({
        mode: this.state.mode === 'edit' ? 'read' : 'edit'
      });
    }
  };

  public handleHarmChange = async (val: string) => {
    if (this.props.characterId) {
      await this.api.updateCharacterByProp('harm', val, this.props.characterId);
      if (this.canSetState) {
        this.setState({
          harm: val
        });
      }
    }
  };

  public handleRoll = (props: {
    rollMod: string;
    rollType: string;
    values: number[];
    moveName: any;
  }) => {
    const roll: RollLog = {
      playerId: this.props.characterId || '',
      rollMod: props.rollMod,
      rollType: props.rollType,
      rollValues: props.values,
      moveName: props.moveName || props.rollType
    };
    this.api.logRoll(roll);
  };

  public handleCred = async (cred: number) => {
    await this.api.updateCharacterByProp(
      'cred',
      cred,
      this.props.characterId || ''
    );
    this.setState({
      cred
    });
  };

  public handleXP = async (experience: number) => {
    await this.api.updateCharacterByProp(
      'experience',
      experience,
      this.props.characterId || ''
    );
    this.setState({
      experience
    });
  };

  public handleMessageUpdate = (
    evt: React.ChangeEvent<HTMLTextAreaElement>
  ) => {
    this.setState({ messageText: evt.target.value });
  };

  public render() {
    const {
      experience,
      player_name,
      playbook,
      body = '',
      contacts = [],
      drones = [],
      skin = '',
      eyes = '',
      wear = '',
      face = '',
      img_url = '',
      cool,
      edge,
      meat,
      mind,
      style,
      synth,
      cred,
      harm,
      mode,
      moves,
      links,
      gangs,
      gear,
      weapons,
      vehicles,
      directives
    } = this.state;
    return (
      <div className="character-container">
        <StyledLink to={'../../'}>Back</StyledLink>

        <h2 style={{ float: 'right' }}>
          {/* <AddBtn onClick={this.toggleMode}>
            {this.state.mode === 'edit' ? 'read' : 'edit'}
          </AddBtn> */}
        </h2>
        <CharacterBasics
          mode={mode}
          player_name={player_name}
          playbook={playbook}
          experience={experience}
          imgURL={img_url || ''}
          onXPChange={this.handleXP}
        />
        <CharacterDetails
          body={body}
          skin={skin}
          eyes={eyes}
          wear={wear}
          face={face}
          mode={mode}
        />
        <Abilities
          mode={mode}
          cool={cool}
          edge={edge}
          meat={meat}
          mind={mind}
          style={style}
          synth={synth}
          handleRoll={this.handleRoll}
        />
        <CredAndHarm
          gear={gear}
          harm={harm}
          mode={mode}
          cred={cred}
          onHarmChange={this.handleHarmChange}
          handleRoll={this.handleRoll}
          handleCredUpdate={this.handleCred}
        />
        <Row>
          <MovesAndCyberware
            moves={moves}
            playerId={this.props.characterId || ''}
            playbook={this.state.playbook}
            cyberware={this.state.cyberware}
          />
        </Row>
        <Row>
          <LinksContactsComponent
            links={links}
            contacts={contacts.map(formatContact)}
          />
          <DirectivesComponent directives={directives} />
        </Row>
        <Row>
          <Weapons weapons={weapons} />
        </Row>
        <Row>
          <Vehicles vehicles={vehicles} drones={drones} />
        </Row>
        <Row>
          <GangsAndCrews gangs={gangs} crews={[]} />
        </Row>
        <Row>
          <CyberdeckComponent />
        </Row>
        <Notes />
      </div>
    );
  }
}
