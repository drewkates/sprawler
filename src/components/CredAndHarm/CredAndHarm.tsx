import React, { Component, FormEvent } from 'react';
import { Col6, Row, IconLabel, NumInput } from '../StyledComponents';
import { GearComponent } from '../GearComponent';
import { HarmComponent } from '../HarmComponent';
import { CharSheetState, Gear } from '../../interfaces';

interface Props {
  cred: number;
  mode: CharSheetState;
  harm: string;
  gear: Gear[];
  onHarmChange: (value: string) => void;
  handleRoll: (props: {
    rollMod: string;
    rollType: string;
    values: number[];
    moveName: any;
  }) => void;
  handleCredUpdate: (cred: number) => void;
}

export class CredAndHarm extends Component<Props> {
  private _onHarmChange(newHarm: string) {
    this.props.onHarmChange(newHarm);
  }
  public onHarmChange = this._onHarmChange.bind(this);

  private _onCredChange(ev: FormEvent<HTMLInputElement>) {
    const value = parseInt(ev.currentTarget.value, 10);
    this.props.handleCredUpdate(value);
  }
  public onCredChange = this._onCredChange.bind(this);

  public render() {
    return (
      <Row
        style={{
          clear: 'both',
          marginTop: '10px',
          paddingBottom: '20px'
        }}
      >
        <Col6>
          <div>
            <IconLabel iconName="cred" htmlFor="attr_cred" customLeftPad="29px">
              Cred
            </IconLabel>
          </div>
          <NumInput
            canEdit={true}
            type="number"
            name="attr_cred"
            value={this.props.cred}
            readOnly={false}
            onChange={this.onCredChange}
          />
          <GearComponent />
        </Col6>
        <HarmComponent
          handleChange={this.onHarmChange}
          handleRoll={this.props.handleRoll}
          level={this.props.harm}
          style={{
            paddingBottom: '20px',
            marginTop: '10px'
          }}
        />
      </Row>
    );
  }
}
