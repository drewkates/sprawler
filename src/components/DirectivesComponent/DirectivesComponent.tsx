import React, { Component, FormEvent } from 'react';
import { Col6, AddBtn, BoldSpan, IconLabel } from '../StyledComponents';
import { Directive } from '../../interfaces';

interface State {
  editMode: boolean;
}
interface Props {
  [key: string]: any;
  directives: Directive[];
}

export class DirectivesComponent extends Component<Props, State> {
  public state: State = {
    // currentDirectiveName: '',
    // currentDirectiveDetails: '',
    editMode: false
  };

  private _toggleEditMode(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    this.setState({
      editMode: !this.state.editMode
    });
  }
  public toggleEditMode = this._toggleEditMode.bind(this);

  // private _handleDirectiveDetailsChange(
  //   ev: FormEvent<HTMLTextAreaElement>
  // ): void {
  //   ev.preventDefault();
  //   this.setState({
  //     currentDirectiveDetails: ev.currentTarget.value
  //   });
  // }

  // public handleDirectiveDetailsChange = this._handleDirectiveDetailsChange.bind(
  //   this
  // );

  // private _handleDirectiveNameChange(ev: FormEvent<HTMLInputElement>): void {
  //   ev.preventDefault();
  //   this.setState({
  //     currentDirectiveName: ev.currentTarget.value
  //   });
  // }
  // public handleDirectiveNameChange = this._handleDirectiveNameChange.bind(this);

  // public _handleAddDirective(ev: FormEvent<HTMLButtonElement>): void {
  //   ev.preventDefault();
  //   const newDirectives = this.state.directives;
  //   if (this.state.currentDirectiveName) {
  //     newDirectives.push({
  //       directiveName: this.state.currentDirectiveName,
  //       directiveDetails: this.state.currentDirectiveDetails
  //     });
  //   }

  //   this.setState({
  //     directives: newDirectives,
  //     currentDirectiveDetails: '',
  //     currentDirectiveName: ''
  //   });
  // }
  // public handleAddDirective = this._handleAddDirective.bind(this);

  public render() {
    return (
      <Col6>
        <IconLabel iconName="directives" htmlFor="attr_directive">
          Directives
        </IconLabel>
        {this.props.directives.map((directive, directiveIndex) => {
          return (
            <div key={directiveIndex}>
              <div>
                <BoldSpan>Name</BoldSpan>:&nbsp;
                <p>{directive.directiveName}</p>
                <BoldSpan>Details:&nbsp;</BoldSpan>
                <p>{directive.details}</p>
              </div>
            </div>
          );
        })}
        {/* <div>
          <div>
            <BoldSpan>Name</BoldSpan>
          </div>
          <div>
            <input
              type="text"
              name="attr_directive"
              value={this.state.currentDirectiveName}
              onChange={this.handleDirectiveNameChange}
            />
          </div>
        </div>
        <div>
          <div>
            <BoldSpan>Details</BoldSpan>
          </div>
          <div>
            <textarea
              name="attr_directive-details"
              cols={50}
              rows={5}
              value={this.state.currentDirectiveDetails}
              onChange={this.handleDirectiveDetailsChange}
            />
          </div>
        </div> */}
        {/* <AddBtn onClick={this.handleAddDirective}>+Add</AddBtn>
        <AddBtn onClick={this.toggleEditMode}>+Modify</AddBtn> */}
      </Col6>
    );
  }
}
