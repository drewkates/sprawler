import React, { Component, FormEvent, MouseEvent } from 'react';
import {
  Col6,
  CyberwareDiv,
  BoldSpan,
  TagDiv,
  DescriptionSpan,
  IconLabel,
  TagDescription
} from '../StyledComponents';
import {
  cyberwareOptions,
  tagDetailsMap
} from '../../cached_data/cyberware_options';
import { TagModal } from '../Modals';
import { CyberWareItem } from '../../interfaces';

interface State {
  showWare: boolean;
  showTag: boolean;
  tagContent?: string;
}
interface Props {
  cyberware: CyberWareItem[];
}

export class Cyberware extends Component<Props, State> {
  public state: State = {
    showWare: true,
    showTag: false
  };
  public _toggleShowWare(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    this.setState({
      showWare: !this.state.showWare
    });
  }
  public toggleShowWare = this._toggleShowWare.bind(this);
  public getCyberwareOptions = () => cyberwareOptions;
  public handleTooltipHover = (ev: MouseEvent<HTMLSpanElement>) => {
    const { currentTarget } = ev;
    const target = ev.target as HTMLElement;
    if (currentTarget instanceof EventTarget && target.tagName === 'SPAN') {
      const firstchild = currentTarget.firstElementChild as HTMLElement;
      if (firstchild && currentTarget.offsetParent) {
        firstchild.style.left = `${currentTarget.offsetLeft}px`;
      }
      currentTarget.classList.add('active');
    }

    if (ev.type === 'mouseleave') {
      currentTarget.classList.remove('active');
    }
  };
  public render() {
    const { cyberware } = this.props;
    return (
      <Col6
        style={{
          paddingBottom: '20px'
        }}
      >
        <IconLabel
          iconName="cyberware"
          customLeftPad="30px"
          htmlFor="attr_cyber"
        >
          Cyberware
        </IconLabel>

        {this.state.showWare
          ? cyberware.map((ware, ind) => {
              return (
                <CyberwareDiv key={ind}>
                  <BoldSpan>{ware.name}</BoldSpan>
                  {ware.tags.map((tag, tagIndex) => {
                    return (
                      <TagDiv
                        key={tagIndex}
                        onMouseEnter={this.handleTooltipHover}
                        onMouseLeave={this.handleTooltipHover}
                      >
                        <TagDescription>
                          {tagDetailsMap[tag].details}
                        </TagDescription>
                        {tag}
                      </TagDiv>
                    );
                  })}
                  <DescriptionSpan>{ware.details}</DescriptionSpan>
                </CyberwareDiv>
              );
            })
          : null}
        {this.state.showTag ? (
          <TagModal>
            <React.Fragment>
              <div>{this.state.tagContent}</div>
            </React.Fragment>
          </TagModal>
        ) : null}
        <div>
          {/*<AddBtn onClick={evtNoop}>+Add</AddBtn>*/}
          {/*<AddBtn onClick={evtNoop}>Modify</AddBtn>*/}
        </div>
      </Col6>
    );
  }
}
