import React from 'react';
import styled from '@emotion/styled';
import { icons } from './icons';
import { Link } from '@reach/router';
import { Collapse } from 'react-collapse';

export const StyledLink = styled(Link)`
  color: #fff;
  text-shadow: 1px 1px 5px white;
  text-decoration: none;
  &:visited {
    color: #fff;
  }
`;

export const ValueSpan = styled.span`
  color: #f0fffc;
  align-self: center;
  padding: 0;
  margin-left: 8px;
`;
export const AbilityLabelWrapper = styled.div`
  flex: 1 1 auto;
  display: flex;
  text-align: center;
  flex-direction: column;
  @media screen and (max-width: 400px) {
    flex-direction: row;
    height: 100px;
    justify-content: space-between;
}
  }
`;

export const AbilityLabel = styled.label`
  color: #ffc109;
  text-shadow: 0 0 7px rgb(255, 167, 77);
  padding: 0;
  + span {
    margin: 0 5px;
  }
`;
export const DescP = styled.p`
  line-height: 14px;
  padding: 0;
`;
export const Span = styled.span`
  line-height: 34%;
  letter-spacing: 1px;
  font-weight: 100;
  margin: 0;
  padding: 0;
  text-align: center;
`;
export const RollBtn = styled.button`
  height: 25px;
  width: 25px;
  border-radius: 50%;
  vertical-align: middle;
`;
export interface IconProps {
  iconName: string;
  customLeftPad?: string;
}

export const IconLabel = styled.label<IconProps>`
  display: inline-block;
  width: auto;
  padding-top: 20px;
  padding-left: ${props => props.customLeftPad || '26px'};
  ${props => icons[props.iconName]};
  width: auto;
`;

export const IntelLabel = styled.label`
  padding: 20px 10px 0 26px;
  background: url(${props => './media/img/labels/intel.png'}) no-repeat scroll -4%
    211% transparent;
  display: inline-block;
  width: auto;
`;
export const AttributeLabel = styled.label`
  display: flex;
  margin: 0 5px 0 0;
  padding: 0;
`;
export const Col6 = styled.div`
  @media (max-width: 400px) {
    width: 100%;
    display: block;
    :first-of-type {
      margin-right: unset;
    }
  }
  @media (max-width: 400px) {
    width: 100%;
    display: block;
    :first-of-type {
      margin-right: unset;
    }
  }
  position: relative;
  width: calc(50% - 15px);
  display: inline-block;
  vertical-align: top;
  :first-of-type {
    margin-right: 30px;
  }
`;
export const InputFull = styled.input`
  width: 100%;
`;
export const TextareaFull = styled.textarea`
  width: 100%;
`;
export const AddBtn = styled.button`
  :first-of-type {
    float: left;
  }
  line-height: 1.5em;
  text-transform: uppercase;
  letter-spacing: 1px;
  padding: 6px;
  color: #94cac2;
  text-shadow: 0 0 14px rgb(0, 255, 187);
  border-color: #4e948b;
  -webkit-transition: all 0.2s;
  -moz-transition: all 0.2s;
  -ms-transition: all 0.2s;
  -o-transition: all 0.2s;
  transition: all 0.2s;
  border-radius: 4px;
  ::focus {
    outline: thin dotted #333;
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px;
  }

  :nth-of-type(2) {
    float: right;
  }
`;
export const AddModCtrl = styled.div`
  margin-top: 10px;
`;
interface RowProps {
  cols?: number;
}

export const Row = styled.div<RowProps>`
  width: 100%;
  border-top: 1px solid #146d69;
  box-shadow: 0px -5px 8px -7px #2fffe2;
  clear: both;
  margin-top: 20px;
  display: block;
  :first-of-type {
    @media screen and (max-width: 400px) {
      margin-top: 40px;
      padding-top: 15px;
    }
    border-top: none;
    box-shadow: none;
  }
`;
export const FlexRow = styled(Row)`
  display: flex;
  flex-wrap: wrap;
`;
export const TagDiv = styled.span`
  margin-left: 5px;
  transition: all 0.2s ease-in;
  cursor: pointer;
  vertical-align: top;
  &.active {
    color: #fff;
    text-shadow: rgb(50, 50, 50) 0px 0px 12px;
  }

  &.active > div {
    opacity: 1;
  }
`;
export const TagDescription = styled.div`
  transition: opacity 0.3s ease-in;
  z-index: 3;
  position: absolute;
  right: 0;
  max-width: 200px;
  bottom: calc(100% - 50px);
  padding: 2px 8px;
  background-color: #146d69;
  opacity: 0;
  cursor: default;
`;
export const CyberwareDiv = styled.div`
  text-transform: uppercase;
  letter-spacing: 2px;
  line-height: 1.5rem;
  font-weight: 100;
  margin-top: 6px;
  margin-bottom: 20px;
`;
export const BoldSpan = styled.b`
  color: white;
`;
export const DescriptionSpan = styled.span`
  color: white;
  > div {
    max-width: 100%;
  }

  * {
    color: white;
    line-height: 2;
  }
`;
export interface EditableInputProps {
  canEdit: boolean;
}

export const EditableInput = styled.input<EditableInputProps>`
  text-shadow: ${props => (props.canEdit ? 'inherit' : 'none')};
  background: ${props => (props.canEdit ? '#123138' : 'none')};
  ${props => (props.canEdit ? 'border-bottom: 1px solid #fofffc;' : '')}
  pointer-events: ${props => (props.canEdit ? 'initial' : 'none')};
`;
export const NumInput = styled(EditableInput)`
  -moz-appearance: ${props => (props.canEdit ? 'normal' : 'textfield')};
  &::-webkit-inner-spin-button,
  &::-webkit-outer-spin-button {
    -webkit-appearance: ${props => (props.canEdit ? 'normal' : 'none')};
    margin: ${props => (props.canEdit ? '5px' : 0)};
  }
`;
export const AbilityInput = styled(NumInput)`
  width: ${props => (props.canEdit ? '3' : '2')} em;
  padding: 4px;
  vertical-align: middle;
`;
export interface DrawerProps {
  showContent: boolean;
}

export const Drawer = styled.div<DrawerProps>`
  display: ${props => (props.showContent ? 'block' : 'none')};
`;
export const Col4 = styled.div`
  display: flex;
  justify-self: space-between;
  flex: 1 1 auto;
  align-items: center;
  :nth-of-type(1) {
    flex: 4 1 auto;
  }

  :nth-of-type(4) {
    flex: 5 1 auto;
    padding-right: 0;
    padding-left: 0;
    margin-right: 0;
  }
`;
export const InlineAttribute = styled.div`
  display: flex;
  flex: 1 1 auto;
  justify-self: start;
  :nth-of-type(4) {
    flex: 2 1 auto;
  }

  align-items: center;
`;
export const CustomSelect = styled.select`
  display: inline-block;
  width: 50%;
  height: auto;
  vertical-align: middle;
  text-align: center;
  line-height: 18px;
  padding: 4px;
`;
export const Code = styled.code`
  color: red;
`;
export const DescriptionContainer = styled.div`
  text-transform: uppercase;
  letter-spacing: 2px;
  line-height: 1.5rem;
  font-weight: 100;
  padding-top: 6px;
  p {
    padding-top: 0;
    line-height: 1.75;
  }
`;

export const HarmRadioLabel = styled.label`
  height: 0px;
  width: 0px;
  border-radius: 0;
  border: 1px #44e6cf solid;
  display: inline-block;
  line-height: 10px;
  text-align: center;
  color: black;
  cursor: pointer;
  background: none;
  position: relative;
  padding: 13px 0px 0px 13px;
  margin: 0;
  box-shadow: 0px 0px 24px -3px #43e6cf;
  left: -12px;
  ::before {
    opacity: 1;
    position: absolute;
    top: 1px;
    left: 1px;
    height: 11px;
    width: 11px;
    background: #229a91;
    content: '';
    opacity: 0;
    box-shadow: 0 0 24px -3px #43e6cf;
  }
`;

export const RollBar = styled(Collapse)<any>`
  max-width: 50%;
  position: absolute;
  right: 0;
  top: 0;
  background-color: #229a91;

  td {
    text-align: right;
  }
`;

export const MoveName = styled(TagDiv)`
  &.active {
    color: red;
  }
`;
