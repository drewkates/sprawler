import links from '../../media/img/labels/links.png';
import cred from '../../media/img/labels/cred.png';
import intel from '../../media/img/labels/intel.png';
import harm from '../../media/img/labels/harm.png';
import moves from '../../media/img/labels/moves.png';
import contacts from '../../media/img/labels/contacts.png';
import cyberware from '../../media/img/labels/cyberware.png';
import directives from '../../media/img/labels/directives.png';
import equipment from '../../media/img/labels/equipment.png';
import weapons from '../../media/img/labels/weapons.png';
import vehicles from '../../media/img/labels/vehicles.png';
import drones from '../../media/img/labels/drones.png';
import programs from '../../media/img/labels/programs.png';
import gangs from '../../media/img/labels/gangs.png';
import crews from '../../media/img/labels/crews.png';
import notes from '../../media/img/labels/notes.png';
import cyberdeck from '../../media/img/labels/cyberdeck.png';

// prettier-ignore-start
export const icons: { [key: string]: string } = {
  links: `background: url(${links}) no-repeat scroll -12% 211% transparent;`,
  cred: `background: url(${cred}) no-repeat scroll -14% 211% transparent;`,
  intel: `background: url(${intel}) no-repeat scroll -4% 211% transparent;`,
  harm: `background: url(${harm}) no-repeat scroll -17% 211% transparent;`,
  moves: `background: url(${moves}) no-repeat scroll -12% 211% transparent;`,
  contacts: `background: url(${contacts}) no-repeat scroll -12% 211% transparent;`,
  cyberware: `background: url(${cyberware}) no-repeat scroll -5% 164% transparent;`,
  directives: `background: url(${directives}) no-repeat scroll -8% 272% transparent;`,
  equipment: `background: url(${equipment}) no-repeat scroll -2% 190% transparent;`,
  weapons: `background: url(${weapons}) no-repeat scroll -12% 240% transparent;`,
  vehicles: `background: url(${vehicles}) no-repeat scroll -3% 392% transparent;`,
  drones: `background: url(${drones}) no-repeat scroll -2% 166% transparent;`,
  cyberdeck: `background: url(${cyberdeck}) no-repeat scroll -4% 219% transparent;`,
  programs: `background: url(${programs}) no-repeat scroll -0% 217% transparent;`,
  gangs: `background: url(${gangs}) no-repeat scroll -6% 240% transparent;`,
  crews: `background: url(${crews}) no-repeat scroll 5% 165% transparent;`,
  notes: `background: url(${notes}) no-repeat scroll -2% 225% transparent;
`
};
// prettier-ignore-end
