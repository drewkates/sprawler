import React, { Fragment } from 'react';
import { RouteComponentProps } from '@reach/router';
import api from '../../utils/api';
import { StyledLink } from '../StyledComponents';
import { DBRollResult } from '../../interfaces';
import { RollBarComponent } from '../RollBar';
import { ChatLogComponent } from '../MessageBar';
import { Auth } from '../../services/auth.service';

interface PlayerDeets {
  playerId: string;
  playerName: string;
}

export interface State {
  players: PlayerDeets[];
  campaign_id?: string;
  rolls: DBRollResult[];
  msgs: any[];
  lastId?: string;
  lastTimestamp?: string;
  showRolls: boolean;
}

const CharLink = (props: PlayerDeets) => (
  <li key={props.playerId}>
    <StyledLink to={`character/${props.playerId}`}>
      {props.playerName}
    </StyledLink>
  </li>
);

export class Campaign extends React.Component<
  RouteComponentProps<{ campaignId: string; auth: Auth }>,
  State
> {
  public state: State = {
    players: [],
    rolls: [],
    msgs: [],
    showRolls: false
  };
  private api = api;
  public okToSet = true;

  public componentWillUnmount() {
    this.okToSet = false;
  }

  public handleMessageSend = async (msg: string) => {
    const { auth, campaignId } = this.props;
    if (msg && auth && campaignId) {
      this.api.logMessage(msg, auth.username, campaignId);
    }
  };

  public async componentDidMount() {
    const { campaignId } = this.props;
    const campaign = await this.api.fetchCampaignDetails(campaignId);
    if (campaign.players.length && this.okToSet) {
      this.setState({
        players: campaign.players
      });
    }
  }

  public render() {
    const players = this.state.players ? this.state.players : [];
    return (
      <Fragment>
        <hr />
        <h3>Characters:</h3>
        <ul>{players.map(CharLink)}</ul>
        {this.props.children}
        <RollBarComponent
          campaignId={this.props.campaignId || ''}
          players={players}
        />
        <ChatLogComponent
          campaignId={this.props.campaignId || ''}
          handleMessageSend={this.handleMessageSend}
        />
      </Fragment>
    );
  }
}
