export interface Gate {
  min: number;
  max: number;
  result: string;
}

export type MoveNames =
  | 'acquire agricultural property'
  | 'act under pressure'
  | 'apply first aid'
  | 'assess'
  | 'declare a contact'
  | 'fast talk'
  | 'get the job'
  | 'getting paid'
  | 'go under the knife'
  | 'harm'
  | 'help or interfere'
  | 'hit the street'
  | 'mix it up'
  | 'play hardball'
  | 'produce equipment'
  | 'research'
  | 'reveal knowledge';

export interface AbilityRoll {
  name: MoveNames;
  gates: Gate[];
}

export const abilityRolls: { [key: string]: AbilityRoll } = {
  'hit the street': {
    name: 'hit the street',
    gates: [
      {
        min: 7,
        max: Infinity,
        result: 'You get what you want.'
      },
      {
        min: 10,
        max: Infinity,
        result:
          'You get a little something extra (choose either [intel] or [gear]).'
      },
      {
        min: 7,
        max: 9,
        result: `
      Choose 2 from the list below:
        * Your request is going to cost you extra
        * Your request is going to take some time to put together
        * Your request is going to attract unwanted attention,complications or consequences
        * Your contact needs you to help them out with something. If you turn them down take -1 ongoing to this
        move till you make it right
      `
      }
    ]
  },
  'act under pressure': {
    name: 'act under pressure',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: 'You do it, no problem'
      },
      {
        min: 7,
        max: 9,
        result: `You stumble, hesitate, or flinch: the MC will offer you a worse outcome, hard bargain, or ugly choice`
      }
    ]
  },
  assess: {
    name: 'assess',
    gates: [
      { min: 10, max: Infinity, result: 'gain 3 hold' },
      { min: 7, max: 9, result: 'gain 1 hold' },
      {
        min: 0,
        max: Infinity,
        result: `In the ensuing action, you may spend 1 hold at any time to
      ask the MC a question from the list below if your examination
      could have revealed the answer. The MC may ask you questions
      to clarify your intent. Take +1 forward when acting on the
      answers.
      Ђ What potential complication do I need to be wary of?
      Ђ What do I notice despite an effort to conceal it?
      Ђ How is ______ vulnerable to me?
      Ђ How can I avoid trouble or hide here?
      Ђ What is my best way in/way out/way past?
      Ђ Where can I gain the most advantage?
      Ђ Who or what is my biggest threat?`
      }
    ]
  },
  'fast talk': {
    name: 'fast talk',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: `NPCs do what you want. PCs choose whether to do it or
    not. If they do, they mark experience. If they don’t, they must
    act under pressure to go against your stated wishes`
      },
      {
        min: 7,
        max: 9,
        result: `NPCs do it, but someone will find out: the MC will advance
        the appropriate Countdown Clock. For PCs choose one:
        Ђ If they do what you want, they mark experience
        Ђ If they don’t do it, they must act under pressure to go
        against your stated wishes
        Then its up to them.`
      }
    ]
  },
  'mix it up': {
    name: 'mix it up',
    gates: [
      {
        min: 7,
        max: Infinity,
        result: `you achieve your objective`
      },
      {
        min: 7,
        max: 9,
        result: ` choose 2:
        Ђ you make too much noise. Advance the relevant Mission
        Clock
        Ђ you take harm as established by the fiction
        Ђ an ally takes harm as established by the fiction
        Ђ something of value breaks`
      }
    ]
  },
  'play hardball': {
    name: 'play hardball',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: ` NPCs do what you want. PCs choose: do what you want,
        or suffer the established consequences`
      },
      {
        min: 7,
        max: 9,
        result: `For NPCs, the MC chooses 1:
        Ђ they attempt to remove you as a threat, but not before
        suffering the established consequences
        Ђ they do it, but they want payback. Add them as a Threat
        Ђ they do it, but tell someone all about it. Advance the
        appropriate Mission Clock
        PCs choose: do what you want, or suffer the established
        consequences. They gain +1 forward to act against you.`
      }
    ]
  },
  research: {
    name: 'research',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: ` choose 2:
        Ђ you make too much noise. Advance the relevant Mission
        Clock
        Ђ you take harm as established by the fiction
        Ђ an ally takes harm as established by the fiction
        Ђ something of value breaks`
      },
      {
        min: 7,
        max: 9,
        result: `take [intel]; the MC will answer your question`
      },
      {
        min: -Infinity,
        max: 6,
        result: `the MC will answer your question and make a move`
      }
    ]
  },
  'help or interfere': {
    name: 'help or interfere',
    gates: [
      {
        min: 7,
        max: Infinity,
        result: `On a hit they take +1 or -2 forward, your choice`
      },
      {
        min: 7,
        max: 9,
        result: `You are implicated in the results of the other character’s
        move and may expose yourself to danger, retribution, or cost`
      }
    ]
  },
  'go under the knife': {
    name: 'go under the knife',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: `The operation was a complete success`
      },
      {
        min: 7,
        max: 9,
        result: ` the cyberware doesn’t work as well as advertised,
        choose one: +unreliable, +substandard, +hardware decay,
        +damaging`
      },
      {
        min: -Infinity,
        max: 6,
        result: `there have been... complications`
      }
    ]
  },
  harm: {
    name: 'harm',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: `: choose 1:
        Ђ you’re out of action: unconscious, trapped, incoherent or
        panicked
        Ђ take the full harm of the attack, before it was reduced by
        armour; if you already took the full harm of the attack,
        take +1-harm
        Ђ lose the use of a piece of cyberware until you can get it
        repaired
        Ђ lose a body part (arm, leg, eye)`
      },
      {
        min: 7,
        max: 9,
        result: `the MC will choose 1:
        Ђ you lose your footing
        Ђ you lose your grip on whatever you’re holding
        Ђ you lose track of someone or something you’re
        attending to
        Ђ someone gets the drop on you`
      }
    ]
  },
  'apply first aid': {
    name: 'apply first aid',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: ` if their Harm Clock is at 2100 or less, reduce their harm
        by two segments. If their Harm Clock is at more than 2100,
        reduce their harm by one segment`
      },
      {
        min: 7,
        max: 9,
        result: ` if their Harm Clock is at 2100 or less, reduce their harm
        by two segments. If their Harm Clock is at more than 2100,
        reduce their harm by one segment`
      }
    ]
  },
  'acquire agricultural property': {
    name: 'acquire agricultural property',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: `You survive until the medics arrive`
      },
      {
        min: 7,
        max: 9,
        result: `you survive at a cost. Pick one: owned, substandard
        treatment (-1 to a stat), cyberware damage (give one piece of
        cyberware a negative tag)`
      },
      {
        min: -Infinity,
        max: 6,
        result: `You bleed out on the street`
      }
    ]
  },
  'get the job': {
    name: 'get the job',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: `choose 3 from the list below`
      },
      {
        min: 7,
        max: 9,
        result: `choose 3 from the list below`
      },
      {
        min: -Infinity,
        max: Infinity,
        result: `
      Ђ the employer provides useful information ([intel])
      Ђ the employer provides useful assets ([gear])
      Ђ the job pays well
      Ђ the meeting doesn’t attract attention
      Ђ the employer is identifiable`
      }
    ]
  },
  'getting paid': {
    name: 'getting paid',
    gates: [
      {
        min: 10,
        max: Infinity,
        result: ` choose 3 from the list below`
      },
      {
        min: 7,
        max: 9,
        result: `choose 1 from the list below`
      },
      {
        min: -Infinity,
        max: Infinity,
        result: `
        Ђ It’s not a set-up or an ambush
        Ђ You are paid in full
        Ђ The employer is identifiable
        Ђ The meeting doesn’t attract the attention of outside parties
        Ђ You learned something from the mission; everyone marks experience
`
      }
    ]
  }
};
