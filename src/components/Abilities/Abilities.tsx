import React, {
  Component,
  FormEvent,
  ReactNode,
  Fragment,
  MouseEvent as ReactMouseEvent
} from 'react';
import {
  AbilityLabel,
  AbilityLabelWrapper,
  AbilityInput,
  RollBtn,
  ValueSpan,
  FlexRow,
  MoveName,
  Span,
  BoldSpan
} from '../StyledComponents';
import { Modal } from '../Modals';
import { ModalWrapper } from '../Moves/MovesAndCyberware';
import { ReactElementLike } from 'prop-types';
import { Attributes, CharSheetState, RollResult } from '../../interfaces';
import { abilityRolls, Gate } from './ability_rolls';
const add = (a: number, b: number): number => a + b;

export type Props = Attributes & {
  mode: CharSheetState;
  handleRoll: (props: any) => void;
};
export interface State {
  currentRoll: number;
  showModal: boolean;
  rollOutcome: string;
  lastRollResult?: RollResult;
  lastRollAbility?: keyof Attributes;
  moveName?: string;
}
export class Abilities extends Component<Props, State> {
  public state: State = {
    showModal: false,
    currentRoll: 0,
    rollOutcome: ''
  };

  public rollSync(): RollResult {
    const rolls = Array(2)
      .fill(1)
      .map(() => Math.ceil(Math.random() * 6));
    return {
      id: 1,
      result: {
        random: { data: rolls }
      }
    };
  }

  public getResult(name: string, num: number) {
    if (!abilityRolls[name.toLowerCase()]) {
      return '';
    }
    return abilityRolls[name.toLowerCase()].gates
      .filter(gate => {
        const gateApplies = num <= gate.max && num >= gate.min;
        return gateApplies;
      })
      .map(gate => gate.result)
      .join(`<br/>`);
  }

  public roll20Sync(key: keyof Attributes, moveName?: string): void {
    const res = this.rollSync();
    const newValue: number = res.result.random.data.reduce(
      add,
      this.props[key]
    );
    const outcome = this.getResult(moveName || '', newValue);

    this.setState({
      currentRoll: newValue,
      showModal: true,
      lastRollResult: res,
      lastRollAbility: key,
      moveName: moveName || key,
      rollOutcome: outcome
    });

    this.props.handleRoll({
      rollType: key,
      values: res.result.random.data,
      rollMod: this.props[key],
      moveName: moveName || key
    });
  }

  private validateRollResult(res: RollResult): boolean {
    return (
      res &&
      res.result &&
      res.result.random &&
      res.result.random.data &&
      res.result.random.data.length >= 1
    );
  }

  public rollAttr = (attr: keyof Attributes, moveName: string | boolean) => {
    const move: string = typeof moveName === 'boolean' ? attr : moveName;
    this.roll20Sync(attr, move);
  };
  public _toggleModal(ev: FormEvent<HTMLButtonElement>): void {
    ev.preventDefault();
    this.setState({
      showModal: !this.state.showModal
    });
  }
  public toggleModal = this._toggleModal.bind(this);
  public handleModalCancel = (ev: KeyboardEvent | MouseEvent): void => {
    if (
      ev instanceof KeyboardEvent &&
      ev.keyCode === 27 &&
      this.state.showModal
    ) {
      this.setState({ showModal: false });
    }
  };
  public handleTooltipHover = (ev: ReactMouseEvent<HTMLSpanElement>) => {
    const { currentTarget } = ev;
    const target = ev.target as HTMLElement;
    if (currentTarget instanceof EventTarget && target.tagName === 'SPAN') {
      const firstchild = currentTarget.firstElementChild as HTMLElement;
      if (firstchild && currentTarget.offsetParent) {
        firstchild.style.left = `${currentTarget.offsetLeft}px`;
      }
      currentTarget.classList.add('active');
    }

    if (ev.type === 'mouseleave') {
      currentTarget.classList.remove('active');
    }
  };
  public getLastRollResult(): ReactElementLike | null {
    const lastRoll = this.state.lastRollResult;
    const lastAbility = this.state.lastRollAbility;
    if (!lastRoll || !lastAbility) {
      return null;
    }

    const isValid = this.validateRollResult(lastRoll);
    if (isValid) {
      return (
        <React.Fragment>
          <tr>
            <td>
              <ValueSpan>{lastAbility} modifier</ValueSpan>
            </td>
            <td>
              <ValueSpan>{this.props[lastAbility]}</ValueSpan>
            </td>
          </tr>
          {lastRoll.result.random.data.map((el, ind) => (
            <tr key={ind}>
              <td>
                <ValueSpan>Roll #{ind}</ValueSpan>
              </td>
              <td>
                <ValueSpan>{el}</ValueSpan>
              </td>
            </tr>
          ))}
        </React.Fragment>
      );
    } else {
      return null;
    }
  }
  private getModalByState(): React.ReactNode {
    return this.state.showModal ? (
      <Modal onCancel={this.handleModalCancel}>
        <ModalWrapper>
          <button
            style={{
              cursor: 'pointer',
              position: 'absolute',
              top: 0,
              right: 0
            }}
            onClick={this.toggleModal}
          >
            X
          </button>
          <BoldSpan>{this.state.moveName}</BoldSpan>
          <table>
            <thead>
              <tr />
              <tr />
            </thead>
            <tbody>{this.getLastRollResult()}</tbody>
          </table>
          <h3>result: {this.state.currentRoll}</h3>
          <Span>{this.state.rollOutcome}</Span>
        </ModalWrapper>
      </Modal>
    ) : null;
  }

  public getInputByEditState(
    attr: keyof Attributes,
    value: number,
    moveNames: string[]
  ): ReactNode {
    const canEdit = this.props.mode === 'edit';
    const displayEl = canEdit ? (
      <AbilityInput
        canEdit={true}
        name={`attr_${attr}`}
        type="number"
        readOnly={true}
        value={value}
      />
    ) : (
      <ValueSpan>{value}</ValueSpan>
    );
    return (
      <AbilityLabelWrapper>
        <div>
          <AbilityLabel htmlFor={`attr-${attr}`} className="attr-label">
            {attr}
          </AbilityLabel>
          {displayEl}
          <RollBtn
            className="roll-btn"
            data-attribute={attr}
            onClick={this.rollAttr.bind(this, attr, false)}
          />
        </div>
        <div>
          {moveNames.map((moveName, ind) => (
            <Fragment key={moveName}>
              {ind !== 0 ? <br /> : null}
              <MoveName
                onMouseLeave={this.handleTooltipHover}
                onMouseEnter={this.handleTooltipHover}
                onClick={this.rollAttr.bind(this, attr, moveName)}
              >
                {moveName}
              </MoveName>
            </Fragment>
          ))}
        </div>
      </AbilityLabelWrapper>
    );
  }
  public render() {
    const { cool, edge, meat, mind, style, synth } = this.props;
    return (
      <FlexRow>
        {this.getModalByState()}
        {this.getInputByEditState('cool', cool, [
          'Act Under Pressure',
          'Apply First Aid'
        ])}
        {this.getInputByEditState('edge', edge, ['Assess', 'Play Hardball'])}
        {this.getInputByEditState('meat', meat, [
          'Mix It Up',
          'Acquire Agri Property'
        ])}
        {this.getInputByEditState('mind', mind, ['Research'])}
        {this.getInputByEditState('style', style, [
          'Fast Talk',
          'Hit The Street'
        ])}
        {this.getInputByEditState('synth', synth, ['Use Cyberware'])}
      </FlexRow>
    );
  }
}
