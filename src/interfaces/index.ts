import { ReactElementLike } from 'prop-types';
import { Vehicle } from '../components/Vehicles';

export interface Gear {
  details: string;
}
export interface CyberwareOpt {
  name: string;
  details: ReactElementLike;
}
export interface CyberWareItem {
  name: string;
  details: ReactElementLike;
  tags: string[];
}
export interface ClientMove {
  name: string;
  notes: ReactElementLike;
}

export interface DBMove {
  id: string;
  move_name: string;
  associated_playbooks: string[];
}

export interface Characteristics {
  eyes: string;
  face: string;
  body: string;
  wear: string;
  skin: string;
  img_url?: string;
}
export interface Attributes {
  cool: number;
  edge: number;
  meat: number;
  mind: number;
  style: number;
  synth: number;
}
export type CharSheetState = 'read' | 'edit';

export interface ContactDB {
  contact_name: string;
  contact_description: string;
  contact_background: string;
  id: string;
}
export interface ContactDetails {
  name: string;
  description: string;
  background: string;
  gang_affiliation?: string;
}

export interface Gang {
  [key: string]: string;
  id: string;
  gangSize: string;
  gangType: string;
  details: string;
  gangName: string;
}

export interface Crew {
  disaster: string;
  crewName: string;
  crewType: string;
  id: string;
  profit: string;
}

export interface Drone {
  name: string;
  style: string;
  frame: string;
  strengths: string[];
  sensors: string[];
  weaknesses: string[];
  armed: string;
  imageURL?: string;
}

export interface CharacterState extends Characteristics, Attributes {
  cred: number;
  contacts: ContactDB[];
  cyberware: any[];
  directives: Directive[];
  drones: Drone[];
  experience: number;
  crews: Crew[];
  gangs: Gang[];
  gear: Gear[];
  harm: string;
  id: string;
  links: LinksDB[];
  moves: ClientMove[];
  playbook: string;
  player_name: string;
  vehicles: Vehicle[];
  weapons: any[];
}
export interface LinksDB {
  playerName: string;
  quant: number;
}

export interface LinksClient {
  fromPlayer: string;
  toPlayer: string;
}

export interface Weapon {
  tags: string[];
  harmRating: string;
  rangeTag: string;
  name: string;
}

export interface Directive {
  directiveName: string;
  details: string;
  id: string;
}

export interface CampaignDisplay {
  id: string;
  campaignName: string;
  players: Array<{ playerName: string; playerId: string }>;
}

export interface DBRollResult {
  id: string;
  roll_type: string;
  roll_mod: number;
  roll_move_name: string;
  values: number[];
  created_at: Date;
  player_id: string;
  user_acct_id?: any;
  campaign_id: string;
}

export interface DisplayRoll {
  id: string;
  ability: string;
  total: number;
  rolls: number[];
  mod: number;
  created_at: Date;
  playerName: string;
  moveName: string;
}

export interface DBRollResponse {
  rolls: DBRollResult[];
  lastId: string;
  lastTimestamp: string;
}

export interface RollResult {
  jsonrpc?: string;
  result: {
    random: {
      data: number[];
      completionTime?: string;
    };
    bitsUsed?: number;
    bitsLeft?: number;
    requestsLeft?: number;
    advisoryDelay?: number;
  };
  id: number;
}
