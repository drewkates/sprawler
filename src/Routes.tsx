import React, { Fragment } from 'react';
import { Router, RouteComponentProps, Link } from '@reach/router';
import { Campaign } from './components/Campaign';
import { Character } from './components/Character';
import { Callback } from './components/Callback';
import { App } from './components/App';
import { Auth } from './services/auth.service';
const auth = new Auth();

const Header = (props: any) => {
  return (
    <ul>
      <li>
        <Link to="/">Home</Link>
        {props.children}
      </li>
    </ul>
  );
};
const CampRoot = (props: any) => (
  <Fragment>
    <Header />
    {props.children}
  </Fragment>
);

export const routes = (
  <Router>
    <App path="/" auth={auth} />
    <Callback path="callback" auth={auth} />
    <CampRoot path="campaign">
      <Campaign path=":campaignId/" auth={auth}>
        <Character path="/character/:characterId" />
      </Campaign>
    </CampRoot>
  </Router>
);
