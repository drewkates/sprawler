import io from 'socket.io-client';

const apiHost = process.env.API_HOST || 'http://localhost:8083';
export const rollsSocket = io(apiHost + '/rolls');
export const msgsSocket = io(apiHost + '/messages');
