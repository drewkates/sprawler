import { FormEvent } from 'react';

export function evtNoop(ev: FormEvent) {
  ev.preventDefault();
  return false;
}
