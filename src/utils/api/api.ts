import { MoveDetails } from '../../cached_data/move_details';
import { ReactElementLike } from 'prop-types';
import {
  CyberWareItem,
  ClientMove,
  CharacterState,
  DBMove,
  LinksDB,
  Weapon,
  Directive,
  ContactDB,
  Gang,
  Crew,
  CampaignDisplay,
  DBRollResult,
  DBRollResponse
} from '../../interfaces';
const apiHost = process.env.API_HOST;

/**
 * This is pretty much just doing rp-like requests for me.
 * I needed something lightweight and didn't feel like doing the same thing over and over
 */
async function rp<T>(url: string, payload?: any, method?: string): Promise<T> {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.addEventListener('load', () => {
      try {
        resolve(JSON.parse(req.response));
      } catch (e) {
        reject(e);
      }
    });
    const reqMethod = method || (payload ? 'POST' : 'GET');
    if (payload) {
      req.open(reqMethod, url);
      req.setRequestHeader('content-type', 'application/json');
      req.send(JSON.stringify(payload));
    } else {
      req.open(reqMethod, url);
      req.send();
    }
  });
}

export function parseDBMoves(move: DBMove): ClientMove {
  return {
    name: move.move_name,
    notes: MoveDetails.get(move.move_name) as ReactElementLike
  };
}
export interface RollLog {
  rollType: string;
  rollMod: string;
  rollValues: number[];
  playerId: string;
  moveName?: string;
}

class Api {
  private apiHost = apiHost;
  public async fetchCampaignDetails(
    campaignId: string = ''
  ): Promise<CampaignDisplay> {
    const url = `${this.apiHost}/campaign/${campaignId}`;
    try {
      return await rp(url);
    } catch (error) {
      console.error('Failed to fetch Character', error);
      return Promise.resolve({ id: '', campaignName: '', players: [] });
    }
  }
  public async fetchCharacter(id: string): Promise<CharacterState | void> {
    const url = `${this.apiHost}/player/${id}`;
    try {
      return await rp(url);
    } catch (error) {
      console.error('Failed to fetch Character', error);
      return void 0;
    }
  }

  public async fetchDetail<T>(playerId: string, fieldName: string): Promise<T> {
    const url = `${this.apiHost}/player/${playerId}/${fieldName}`;
    return await rp(url);
  }
  public async fetchPlayerCyberware(id: string): Promise<CyberWareItem[]> {
    return this.fetchDetail<CyberWareItem[]>(id, 'cyberware');
  }
  public async fetchCampaign(): Promise<CampaignDisplay[]> {
    return await rp(`${this.apiHost}/campaign`);
  }
  public async fetchPlayerMoves(id: string): Promise<DBMove[]> {
    return this.fetchDetail<DBMove[]>(id, 'move');
  }
  public async fetchPlayerLinks(id: string): Promise<LinksDB[]> {
    return this.fetchDetail<LinksDB[]>(id, 'links');
  }
  public async fetchPlayerWeapons(id: string): Promise<Weapon[]> {
    return this.fetchDetail<Weapon[]>(id, 'weapon');
  }
  public async fetchPlayerContacts(id: string): Promise<ContactDB[]> {
    return this.fetchDetail<ContactDB[]>(id, 'contact');
  }
  public async fetchPlayerGangs(id: string): Promise<Gang[]> {
    return this.fetchDetail<Gang[]>(id, 'gang');
  }
  public async fetchPlayerCrews(id: string): Promise<Crew[]> {
    return this.fetchDetail<Crew[]>(id, 'crew');
  }
  public async fetchPlayerDirectives(id: string): Promise<Directive[]> {
    return this.fetchDetail<Directive[]>(id, 'directive');
  }
  public async fetchMovesByPlaybook(pb: string): Promise<ClientMove[]> {
    const url = `${this.apiHost}/move/?playbook=${pb}`;
    const moves = (await rp(url)) as DBMove[];
    if (moves && moves.length) {
      return moves.map<ClientMove>(parseDBMoves);
    } else {
      return [];
    }
  }
  public async updateCharacterByProp(
    prop: string,
    value: string | number,
    id: string
  ): Promise<CharacterState> {
    const url = `${this.apiHost}/player/${id}`;
    return rp(
      url,
      {
        [prop]: value
      },
      'PATCH'
    );
  }

  public async logRoll(params: RollLog) {
    rp(`${this.apiHost}/rolls`, { ...params });
  }
  public async fetchRolls(
    id: string,
    lastId?: string,
    lastTimestamp?: string,
    limit?: number
  ) {
    let url = `${this.apiHost}/rolls?campaignId=${id}`;
    if (lastId && lastTimestamp) {
      url += `&lastId=${lastId}&lastTimestamp=${lastTimestamp}`;
    }

    if (limit) {
      url += `&limit=${limit}`;
    }
    return rp<DBRollResponse>(url);
  }

  public async fetchMessages(
    id: string,
    lastId?: string,
    lastTimestamp?: string,
    limit?: number
  ) {
    let url = `${this.apiHost}/messages?campaignId=${id}`;
    if (lastId && lastTimestamp) {
      url += `&lastId=${lastId}&lastTimestamp=${lastTimestamp}`;
    }

    if (limit) {
      url += `&limit=${limit}`;
    }
    return rp<any>(url);
  }

  public async logMessage(
    messageText: string,
    playerName: string,
    campaignId: string
  ) {
    const url = `${this.apiHost}/messages`;
    return rp<any>(url, { messageText, playerName, campaignId });
  }
}

export default new Api();
