import React from 'react';
import { render } from 'react-dom';
import { routes } from './Routes';

class Root extends React.Component {
  public render() {
    return (
      <div id="app">
        <div id="containerdiv">{routes}</div>
      </div>
    );
  }
}

render(<Root />, document.getElementById('root'));
