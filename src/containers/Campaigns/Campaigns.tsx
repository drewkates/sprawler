import React, { Component, Fragment } from 'react';

import api from '../../utils/api';
import { StyledLink } from '../../components/StyledComponents';
import { CampaignDisplay } from '../../interfaces';

const CampaignLink = (props: CampaignDisplay) => (
  <li key={props.id}>
    <StyledLink key={props.id} to={'/campaign/' + props.id}>
      {props.campaignName}
    </StyledLink>
  </li>
);

export interface State {
  campaigns: CampaignDisplay[];
}

export class Campaigns extends Component<{}, State> {
  private api = api;
  public okToSet = true;
  public state: State = { campaigns: [] };
  public async componentDidMount() {
    const campaigns = await this.api.fetchCampaign();
    if (campaigns.length && this.okToSet) {
      this.setState({
        campaigns
      });
    }
  }
  public render() {
    const campaigns = this.state.campaigns;
    const CampaignList = campaigns.map(CampaignLink);
    return (
      <Fragment>
        <h2>Campaigns:</h2>
        {CampaignList}
      </Fragment>
    );
  }
}
