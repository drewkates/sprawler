import React from 'react';
import { ReactElementLike } from 'prop-types';
import {
  BoldSpan,
  DescriptionSpan,
  Code,
  DescriptionContainer
} from '../components/StyledComponents';

export const MoveDetails = new Map<string, ReactElementLike>();

MoveDetails.set(
  'Ear to the ground',
  <DescriptionContainer>
    <p>
      You have a knack for loosening lips and picking up information. When you
      circulate among a neighbourhood or a group of people, you may&nbsp;
      <strong>research</strong> to gather information.
    </p>
  </DescriptionContainer>
);

MoveDetails.set(
  'It all fits together',
  <DescriptionContainer>
    <p>
      You’re a master of making connections between seemingly unrelated events.
      At the start of a mission, roll <BoldSpan>&nbsp;Edge</BoldSpan>.
    </p>
    <div>
      <BoldSpan>10+:&nbsp;</BoldSpan>
      <DescriptionSpan>gain 3 hold</DescriptionSpan>
    </div>
    <div>
      <BoldSpan>7-9:&nbsp;</BoldSpan>
      <DescriptionSpan>gain 1 hold</DescriptionSpan>
    </div>
    <p>
      As you put everything together during the mission, spend 1 hold at any
      time to ask a question from the research list.
    </p>
  </DescriptionContainer>
);

MoveDetails.set(
  'Eye for detail',
  <DescriptionContainer>
    <p>
      You are a master at tailing people and staking out locations. When you
      perform surveillance on a person or a place, gain <Code>[intel]</Code> and
      roll &nbsp;<BoldSpan>assess</BoldSpan>
    </p>
  </DescriptionContainer>
);
MoveDetails.set('See the angles', <DescriptionContainer />);

MoveDetails.set(
  'Believers',
  <DescriptionContainer>
    <p>
      You are part of a gang, tribe, band, corporation or similar group. You can
      go to them for aid, for resources or to hide out until the heat dies down.
      As a group, they’re pretty trustworthy, but they will make demands on you
      in return (your gang counts as a <BoldSpan>&nbsp;Contact</BoldSpan>). By
      default this group has a core of about 20 people as well as various
      associates and groupies
    </p>
    <BoldSpan>See Gangs</BoldSpan>
  </DescriptionContainer>
);
MoveDetails.set(
  'Vision thing',
  <DescriptionContainer>
    <p>
      When you have time and space for an emotional connection with someone and
      you passionately advocate for your vision, roll
      <BoldSpan>&nbsp;&nbsp;Style</BoldSpan>.
    </p>
    <div>
      <BoldSpan>10+:&nbsp;</BoldSpan>
      <DescriptionSpan>gain 2 hold</DescriptionSpan>
    </div>
    <div>
      <BoldSpan>7-9:&nbsp;</BoldSpan>
      <DescriptionSpan>gain 1 hold</DescriptionSpan>
    </div>
    <p>Spend 1 hold to have the targeted NPCs:</p>
    <ul>
      <li>give you something you want</li>
      <li>do something you ask</li>
      <li>fght to protect you or your cause</li>
      <li>
        disobey an order given by someone with authority or leverage over them
      </li>
    </ul>
    <p>
      When you use this move on a PC, spend your hold to help or interfere as if
      you had rolled a 10+ (i.e. give them +1 or -2). If you miss against a PC,
      they gain 2 hold against you which they can use in the same way.
    </p>
  </DescriptionContainer>
);
MoveDetails.set(
  'Driven',
  <DescriptionContainer>
    <p>
      When you begin a mission that furthers your vision, roll
      <BoldSpan>Edge</BoldSpan>
    </p>
    <div>
      <BoldSpan>10+:&nbsp;</BoldSpan>
      <DescriptionSpan>gain 3 hold</DescriptionSpan>
    </div>
    <div>
      <BoldSpan>7-9:&nbsp;</BoldSpan>
      <DescriptionSpan>gain 1 hold</DescriptionSpan>
    </div>
    <p>
      You may spend 1 hold before rolling any other move to take +1 or -2
      forward to the move
    </p>
  </DescriptionContainer>
);
MoveDetails.set('See the angles', <DescriptionContainer />);
