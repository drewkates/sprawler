import React from 'react';
import { Code } from '../components/StyledComponents';
import { CyberwareOpt } from '../interfaces';

export interface CyberwareTag {
  name: string;
  details: string;
}
export const tagDetailsMap: { [key: string]: CyberwareTag } = {
  damaging: {
    name: 'damaging',
    details:
      'sometimes it hurts like hell and eventually it will do permanent nerve damage'
  },
  dampening: {
    name: 'dampening',
    details: 'protects against sonic Stun effects.'
  },
  'high speed': {
    name: 'high speed',
    details: 'allows you to stream and access data much faster.'
  },
  implant: {
    name: 'implant',
    details:
      'implant weapons can have the +discreet tag and cannot be taken away without causing harm.'
  },
  'inaccessible partition': {
    name: 'inaccessible partition',
    details:
      'it has a courier mode in which the implantee can’t access the data being recorded, stored or transmitted.'
  },
  jamming: {
    name: 'jamming',
    details:
      'permits the jamming of any communications that lack the +encrypted tag.'
  },
  'light amplifcation': {
    name: 'light amplifcation',
    details: 'allows you to see well even with weak light sources.'
  },
  'multi-tasking': {
    name: 'multi-tasking',
    details: 'you can control multiple vehicles or drones simultaneously.'
  },
  recording: {
    name: 'recording',
    details:
      'you can record the data processed by the device. Large quantities of data may require +high capacity storage.'
  },
  'satellite relay': {
    name: 'satellite relay',
    details:
      'the cyberware can be controlled remotely by someone else. This tag is often installed as a back door by micromanaging corporations.'
  },
  substandard: {
    name: 'substandard',
    details: 'it works, but not as well as it should.'
  },
  thermographic: {
    name: 'thermographic',
    details: 'allows you to see heat patterns in the infrared spectrum.'
  },
  unreliable: {
    name: 'unreliable',
    details: 'sometimes it doesn’t work.'
  },
  'wide frequency': {
    name: 'wide frequency',
    details:
      'allows you to hear sounds beyond the normal range of human hearing'
  },
  encrypted: {
    name: 'encrypted',
    details:
      'it’s resistant to hacking. The MC must make a move to subvert the encryption before making a move to hack this cybernetic device'
  },
  'flare compensation': {
    name: 'flare compensation',
    details: 'protects against visual Stun effects.'
  },
  'high capacity': {
    name: 'high capacity',
    details:
      'greatly increases the storage capacity of the device. This is useful for storing, uploading and transporting large quantities of complex data. You will be able to loot more paydirt from corporate archives and locally store more recorded data'
  },
  'hardware decay': {
    name: 'hardware decay',
    details: 'it works now, but it’s just a matter of time...'
  },
  magnification: {
    name: 'magnification',
    details:
      'allows you to see to much greater ranges. This does not affect weapon accuracy.'
  }
};

export const cyberwareOptions: { [key: string]: CyberwareOpt } = {
  cybereyes: {
    name: 'cybereyes',
    details: (
      <div>
        <span>
          Replacement eyes that grant super-human or extra-human visual
          capabilities. Aesthetic considerations are also important to many
          buyers
        </span>

        <span>
          When your enhanced sight helps, you may roll Synth for
          <strong>assess</strong>
        </span>
      </div>
    )
  },
  cyberears: {
    name: 'cyberears',
    details: (
      <div>
        <span>
          Replacement ears that grant super-human or extra-human aural
          capabilities
        </span>

        <span>
          When your enhanced hearing helps, you may roll Synth for
          <strong>assess</strong>
        </span>
      </div>
    )
  },
  cybercoms: {
    name: 'cybercoms',
    details: (
      <div>
        <span>
          An internal headware communications suite which allows silent,
          thought-activated communications, you may roll Synth for
          <strong>assess</strong>
        </span>
      </div>
    )
  },
  cyberarm: {
    name: 'cyberarm',
    details: (
      <div>
        <p>
          Replacements arms can be prosthetic replacements or elective
          enhancements. The former replace standard human abilities, while the
          latter supplement particular abilities or incorporate tools into the
          human body, often enhancing precision through neuro-mechanical
          control. Cyberarms are available in a variety of aesthetic
          presentations, ranging from overtly synthetic or mechanical to
          visually indistinguishable from human.
        </p>

        <ul>
          <li>
            <strong>Augmented Strength</strong>: +2 harm when using a melee
            weapon that relies on physical strength.
          </li>

          <li>
            <strong>Implant Tools</strong>: When you have time and space to
            interface with a device you are attempting to fix, bypass, or tamper
            with, take +1 forward.
          </li>

          <li>
            <strong>Implant Weaponry</strong>: Either: retractable blades
            (2-harm hand messy implant), a holdout frearm (2 harm close loud
            implant), or a monoflament whip (4-harm hand messy area dangerous
            implant)
          </li>
        </ul>
      </div>
    )
  },
  cyberlegs: {
    name: 'cyberlegs',
    details: (
      <div>
        <p>
          Replacements legs can be prosthetic replacements or elective
          enhancements. Elective models allow super-human athletic abilities,
          especially running speed and jumping distance.
        </p>

        <p>
          When your enhanced athleticism could help you
          <strong>act under pressure</strong>, take +1 forward. If you roll a
          12+ when <strong> acting under pressure </strong>, gain 1 hold which
          you can spend as described in the move
          <strong>assess</strong>
        </p>
      </div>
    )
  },
  'dermal plating': {
    name: 'dermal plating',
    details: (
      <div>
        Sub-dermal plates of synthetic armour placed over critical internal
        organs. When you make the harm move, subtract 2 from your roll. Subtract
        3 from your roll if the harm came from a weapon with the +ﬂechette tag.
      </div>
    )
  },
  'implant weaponry': {
    name: 'implant weaponry',
    details: (
      <div>
        Retractable or internally concealed weapons can be mounted directly into
        the human body with appropriate structural anchoring and heat
        dissipation mechanisms. These include both regular weapons optimised for
        internal use or storage blades as well as specialised weapons like the
        chest-stored, orally-deployed, “cybersnakes” favoured by certain
        corporate honey-trap assassins. Either: Ђ retractable blades (2-harm
        hand messy implant) Ђ a holdout frearm (2-harm close loud implant) Ђ a
        monoflament whip (4-harm hand messy area dangerous implant) Ђ internal
        assassination implant (4-harm intimate slow implant).
      </div>
    )
  },
  'muscle grafts': {
    name: 'muscle grafts',
    details: (
      <div>
        Synthetic fbres are grafted into human muscle to increase muscular
        strength, ﬂexibility, and resilience. » When you mix it up with a melee
        weapon, you may roll Synth instead of Meat and may also inﬂict +1 harm.
      </div>
    )
  },
  'neural interface': {
    name: 'neural interface',
    details: (
      <div>
        A headware interface that translates the brain’s neural signals into
        machine control impulses. This allows a user to control an appropriately
        confgured external device such as a vehicle, mounted weapon, recording
        device, or hacked electronic system at instinctive neural speeds. » You
        may take the Driver move second skin as an advance. » You may take the
        Hacker move jack in as an advance. Choose one of the following options.
        Additional choices can be added to the headware system later in the same
        way as adding a new piece of cyberware. Ђ Data Storage: A neural
        interface that allows speed-of-thought communication between the user’s
        brain and a Matrix-capable computer system. These systems usually
        include a useful quantity of headware storage capacity. When you use
        research to search internally or externally stored data, gain an extra
        <Code> [intel] </Code> on a hit. Choose two of following tags:
        +inaccessible partition, +encrypted, +high capacity, +high speed. Remote
        Control Module: An interface which includes wireless broadcast and
        reception capacity allowing remote control of vehicles and drones. When
        you have a remote control module installed, choose two of following
        tags: +encrypted, +multi-tasking, +inaccessible partition. Ђ Targeting
        Suite: Uses a direct neural link between a hand-held gun and user to
        project targeting information into the user’s vision. When you fre a
        weapon you are +linked to, you may inﬂict additional harm equal to your
        Synth. You may also roll Synth instead of Meat to mix it up. You may
        precisely defne the area of effect for weapons with the +autofre tag to
        exclude or include potential targets from weapon damage.
      </div>
    )
  },
  'synthetic nerves': {
    name: 'synthetic nerves',
    details: (
      <div>
        The replacement of significant parts of the nervous system can
        drastically increase reaction time. Users react so quickly that they can
        almost dodge bullets. » If none of your enemies have synth nerves, take
        +1 forward to mix it up. In situations where reaction time is critical,
        take +1 forward to act under pressure.
      </div>
    )
  },
  skillwires: {
    name: 'skillwires',
    details: (
      <div>
        A headware expert system linked to the brain’s muscle control centres
        that triggers specifc muscular reactions which simulate the instincts
        and actions of an expert practitioner. The system contains a number of
        external slots into which skillchips can be slotted granting a small
        number of skills in parallel. Skillchips often include a knowledge
        database covering non-physical aspects of the programmed skill. » When
        your slotted skillchip is appropriate to a move you are making, take +1
        ongoing if your relevant stat is +1 or less. Standard skillwires comes
        with two slots and you may have one chip active in each slot. If you
        start with Skillwires, you also start with one chip per slot. You can
        acquire more skillchips in play like any other gear. Example skillchips:
        martial arts, breaking and entering, rock climbing, skydiving, scuba
        diving, planning and logistics, frefght combat, extreme driving,
        parkour, frst aid, military history and tactics.
      </div>
    )
  },
  'tactical computer': {
    name: 'tactical computer',
    details: (
      <div>
        An expert system core calculates distance, environment and movement
        factors and provides a suite of tactical tools to enhance the user’s
        understanding of and operation within a tactical environment. » When you
        assess in a tactical situation, hold +1, even on a miss
      </div>
    )
  }
};
