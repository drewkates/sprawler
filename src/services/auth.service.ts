import { WebAuth, Auth0DecodedHash, Auth0Error } from 'auth0-js';
import { navigate } from '@reach/router';
import jwt_decode from 'jwt-decode';

if (
  !process.env.AUTH_CLIENT_ID ||
  !process.env.AUTH_DOMAIN ||
  !process.env.AUTH_REDIRECT
) {
  throw new Error('App started without clientid, domain, or redirect');
}

export class Auth {
  public accessToken = '';
  public idToken = '';
  public expiresAt = 0;

  private auth0 = new WebAuth({
    domain: process.env.AUTH_DOMAIN || '',
    clientID: process.env.AUTH_CLIENT_ID || '',
    redirectUri: process.env.AUTH_REDIRECT,
    audience: `https://${process.env.AUTH_DOMAIN}/userinfo`,
    responseType: 'token id_token',
    scope: 'openid profile'
  });

  get profile(): { nickname: string } {
    if (this.idToken) {
      return jwt_decode(this.getIdToken());
    } else {
      return { nickname: 'anonymous' };
    }
  }

  get username(): string {
    try {
      return this.profile.nickname;
    } catch (e) {
      return 'anonymous';
    }
  }

  constructor() {
    this.getAccessToken = this.getAccessToken.bind(this);
    this.getIdToken = this.getIdToken.bind(this);
    this.setSession = this.setSession.bind(this);
    this.renewSession = this.renewSession.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
  }

  public _handleAuthentication() {
    this.auth0.parseHash(
      (err: Auth0Error | null, authResult: Auth0DecodedHash | null) => {
        if (authResult) {
          this.setSession(authResult);
        } else if (err) {
          navigate('/');
          console.error('error: ', err);
          alert(`Error: ${err.error}. Check the console for further detail.s`);
        }
      }
    );
  }
  public handleAuthentication = this._handleAuthentication.bind(this);

  public getAccessToken() {
    return this.accessToken;
  }

  public getIdToken() {
    return this.idToken;
  }

  public setSession(result: Auth0DecodedHash) {
    localStorage.setItem('isLoggedIn', 'true');

    const expiresAt = (result.expiresIn || 0) * 1000 + new Date().getTime();
    this.accessToken = result.accessToken || '';
    this.idToken = result.idToken || '';
    this.expiresAt = expiresAt;

    navigate('/');
  }

  public renewSession() {
    this.auth0.checkSession({}, (err, authResult: Auth0DecodedHash | null) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        this.logout();
        console.error(err);
        alert(
          `could not get a new token (${err.error}: ${err.error_description}).`
        );
      }
    });
  }

  private _logout() {
    this.accessToken = '';
    this.idToken = '';
    this.expiresAt = 0;

    localStorage.removeItem('isLoggedIn');

    this.auth0.logout({
      returnTo: window.location.origin
    });

    navigate('/');
  }
  public logout = this._logout.bind(this);

  public isAuthenticated() {
    const expiresAt = this.expiresAt || 0;
    return new Date().getTime() < expiresAt;
  }

  public _login() {
    this.auth0.authorize();
  }
  public login = this._login.bind(this);
}
