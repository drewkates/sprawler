import React from 'react';
import { create } from 'react-test-renderer';
import { LandingPage } from '../components/LandingPage';
import { Auth } from '../services/auth.service';

const fakeAuth = new Auth();
test('snapshot', () => {
  const c = create(<LandingPage auth={fakeAuth} />);
  expect(c.toJSON()).toMatchSnapshot();
});
